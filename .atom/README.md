# Atom configuration
To backup:
```
apm list --installed --bare > ~/.atom/packages.txt
```

To restore:

```
apm install --packages-file ~/.atom/packages.txt
```
