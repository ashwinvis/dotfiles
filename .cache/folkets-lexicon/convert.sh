#!/bin/bash
for file in folkets_en_sv_public folkets_sv_en_public
do
  pyglossary $file.xdxf dict/$file.ifo -v4 --read-format=Xdxf --write-format=Stardict '--json-write-options={"merge_syns": "true", "sametypesequence": "x"}' --remove-html=br
done
