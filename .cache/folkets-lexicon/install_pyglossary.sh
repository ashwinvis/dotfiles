#!/usr/bin/bash
set -xeu

# Execute using Python 3.7, 3.8 or 3.9
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
mkdir -p venv/share
ln -rs venv/src/pyglossary venv/share/
