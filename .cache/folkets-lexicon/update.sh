#!/bin/bash
# Fail on error, unbound variables, piped errors
set -euo pipefail

for file in folkets_en_sv_public.xdxf folkets_sv_en_public.xdxf
do
  url=https://folkets-lexikon.csc.kth.se/folkets/$file
  if [ -n "$(diff <(curl -I -s $url | grep Modified) <(grep Modified $file.txt))" ]
  then
    echo "Updates available for $file. Downloading..."
    curl -I $url > "$file.txt"
    curl -LO ${url}
    cat "$file.txt"
  else
    echo "$file is up-to-date."
  fi
done

sed -i 's/English-Swedish/Swedish-Engelska/g' folkets_sv_en_public.xdxf
