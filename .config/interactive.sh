#!/bin/bash
# Config for interactive login/non-login POSIX shells
# Can be sourced in ~/.bashrc, ~/.zshrc etc

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# Return the first pathname in $PATH for name in $1
cmd_exists() {
  if [ "$BASH_VERSION" ]; then
    type -P "$1"  # No output if not in $PATH
  else
    command -v "$1" 2> /dev/null
    # type "$1" 2> /dev/null | grep -v "not found"
  fi
}

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=${HISTSIZE}
# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

#Env Variables
if [ -x /usr/bin/nvim ]; then
  export EDITOR="nvim"
elif [ "$(cmd_exists nvim)" ]; then
  # user installed nvim (AppImage)
  export EDITOR="nvim"
elif [ -x /usr/bin/vim ]; then
  export EDITOR="vim"
elif [ -x /usr/bin/vim.tiny ]; then
  export EDITOR="vim.tiny"
fi
# if [ -x /usr/bin/nvim-qt ]; then
#   export VISUAL="nvim-qt"
# elif [ -x /usr/bin/gvim ]; then
#   export VISUAL="gvim"
# fi
nvo() {
  [ $# -eq 0 ] && printf "Open files using nvim server.\nUsage: nvo file1 file2 ...\n"
  nvr --servername "$(nvr --serverlist | tail -n 1)"
}

[ -x /usr/bin/firefox ] && export BROWSER="firefox"

export GIT_EDITOR="$EDITOR"

# Less
# Minimal
# export LESS='-R'
# Mouse-wheel scrolling has been disabled by -X (disable screen clearing).
# Remove -X and -F (exit if the content fits on one screen) to enable it.
export LESS='-F -g -i -M -R -S -w -X -z-4'

# if [ "$(cmd_exists bat)" ]; then
#   export LESSOPEN="|bat --color always %s"
# elif
if [ "$(cmd_exists pygmentize)" ]; then
  export LESSOPEN='|pygmentize -O style=monokai -g %s'  # pip install pygments
fi
export BAT_THEME='Solarized (dark)'

export KRB5_CONFIG=$HOME/.config/krb5.conf
export KRB5CCNAME=$HOME/.local/tmp/krb5cc
if [ -d /scratch ]; then
  export SCRATCH=/scratch/avmo
else
  export SCRATCH=$HOME/.local/scratch
fi
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/.local/lib
export LIBRARY_PATH=$LIBRARY_PATH:$HOME/.local/lib
export CPATH=$CPATH:$HOME/.local/include

alias open='xdg-open'

alias g="git"
alias nyx='sudo -u tor nyx'
if [ "$(cmd_exists exa)" ]; then
  alias ls="exa --group-directories-first --time-style=iso --color=auto -F"
  alias lt='ls -l --sort oldest'
else
  alias ls='ls --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
  alias lt='ls -l -tr'
fi
alias ll='ls -l'
alias la='ls -la'
alias l='ls -1'
alias diff='diff --color=auto'
alias grep='grep --color=tty -d skip'
alias cp="cp -i"                          # confirm before overwriting something
alias mv="mv -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias v='$EDITOR'
alias vi='$EDITOR'
alias vi-debug='$EDITOR -u ~/.config/vimrc/debug.vim'
alias vimwiki='$EDITOR -c VimwikiIndex'
alias emacs='emacs -nw'
alias less='less -R'
alias info='info --vi-keys'
alias journalctl-errors='journalctl -p err..alert'
alias makectags='ctags -R .'
alias stow='stow -v'
alias xclip-clipboard='xclip -selection clipboard'
alias ssh-keygen='ssh-keygen -C "$(whoami)@$(hostname)-$(date -I)" -t ed25519'
alias feh='feh --auto-zoom -. --image-bg black'
alias feh-shuffle='feh --recursive --randomize'
# Networking
alias wanip='dig +short myip.opendns.com @resolver1.opendns.com'
alias dnsip='nmcli --fields ip4.dns,ip6.dns con show $(nmcli -t --fields NAME con show --active)'
if [ "$(cmd_exists trash-put)" ]; then
  alias rm="trash-put"
else
  alias rm="rm -I"
fi

# ArchLinux
if [ -x /usr/bin/pacman ]; then
  alias fixpacdb='sudo rm -f /var/lib/pacman/db.lck'
  alias pacman-diffs='pacdiff -f'
  alias aurvote='ssh aur@aur.archlinux.org vote'
  alias mksrcinfo='makepkg --printsrcinfo >! .SRCINFO'
  alias reflector='sudo /usr/bin/python -m Reflector'
  alias powerpill='sudo /usr/bin/python -m Powerpill'
  alias pacman-update-mirrors='sudo /usr/bin/python -m Reflector -c SE -c NO -c DK -c NL -f 8 --save /etc/pacman.d/mirrorlist'
  alias pacman-recent="expac --timefmt='%Y-%m-%d %T' '%l\t%n' | sort | less"

  # AUR helper
  alias aur="paru"
  alias aur-repo-add="repo-add $HOME/.cache/makepkg/repo/aur.db.tar.gz"
  alias aur-repo-remove="repo-remove $HOME/.cache/makepkg/repo/aur.db.tar.gz"

  # Chroot
  export CHROOT="$HOME/.cache/chroot"

  checkupdates() {
    if [ $# -gt 0 ]; then
      /usr/bin/checkupdates $@
    else
      /usr/bin/checkupdates | column -t
    fi
  }
fi

# Python
## Uncomment if pip cache takes up too much space
# alias pip="python -m pip --no-cache-dir"
[ "$(cmd_exists pip-pss)" ] && alias pip="pip-pss"
## or even better simply symlink ~/.cache/pip

[ -x /usr/bin/spyder ] && alias spyder='env python /usr/bin/spyder'
[ -x /usr/bin/spyder3 ] && alias spyder3='env python3 /usr/bin/spyder3'
alias jupyter-clean-checkpoints='find -name ".ipynb_checkpoints" -type d | xargs rm -rf'
alias ipcluster-mpi='ipcluster start -n 2 --engines=MPIEngineSetLauncher'

## Install / Uninstall your virtualenvs to appear as kernels, allowing you to launch them from a
## system-wide or user-wide Jupyter Lab / Notebook installation
## Similar functionality is offered by https://github.com/Anaconda-Platform/nb_conda_kernels
alias ipykernel-install='ipython kernel install --user --name=py-$(basename $VIRTUAL_ENV)'
alias ipykernel-uninstall='jupyter kernelspec uninstall py-$(basename $VIRTUAL_ENV)'
alias ipykernel-list='jupyter kernelspec list | grep "\/py.*"'

alias valgrind-python='valgrind --suppressions=~/.config/valgrind/valgrind-python.supp'

alias pip-install="pip-compile && pip-sync"

# fuck the previous executed Python script
alias fuck.py='eval $(quickfix.py --fuck $(fc -ln -1))'
# when there is a library misbehaving, you are expected to say "fuck them all"
alias fuckthemall.py='eval $(quickfix.py --fuck --all $(fc -ln -1))'

# alias java='java -Dsun.java2d.uiScale=2'
# alias matlab-cli='matlab -nojvm -nodisplay -nosplash'
# alias acroread='nohup wine ~/.wine/drive_c/Program\ Files\ \(x86\)/Adobe/Reader\ 11.0/Reader/AcroRd32.exe > /dev/null &'
# alias acroread="nohup wine '.PlayOnLinux/wineprefix/AdobeAcrobatReaderDC/drive_c/Program Files/Adobe/Acrobat Reader DC/Reader/AcroRd32.exe'"

# Flatpak aliases
# alias code='flatpak run com.visualstudio.code.oss'
alias marker='flatpak run com.github.fabiocolacio.marker'
export NEK_SOURCE_ROOT="$HOME/Sources/snek5000/Nek5000"

# LaTeX / tllocalmgr
export TEXMFLOCAL=/usr/local/share/texmf:/usr/share/texmf
export TEXMFHOME=$HOME/.texmf

# Ruby
# FIXME: slow ~0.5s
# if [ "$(cmd_exists ruby)" ] && [ "$(cmd_exists gem)" ]; then
#   PATH="$PATH:$(ruby -r rubygems -e 'puts Gem.user_dir')/bin"
# fi

# FluidDyn
export FLUIDSIM_PATH=$SCRATCH/data
export FLUIDLAB_PATH=$SCRATCH/data
export FLUIDDYN_PATH_SCRATCH=$SCRATCH/tmp
export FLUIDDYN_NUM_PROCS_BUILD=3

# VirtualEnv / Pew
export WORKON_HOME="$SCRATCH/opt"
# source $(pew shell_config)
# source virtualenvwrapper.sh

SDL_VIDEO_FULLSCREEN_HEAD=0

# Dircolors
eval "$(dircolors $HOME/.dir_colors)"

# Firefox
export MOZ_USE_XINPUT2=1

# Alacritty
# FIXME: needed for HIDPI
# export WINIT_HIDPI_FACTOR=2

# Python startup
export PYTHONSTARTUP=$HOME/.config/pythonstartup.py

# Jupyter
export JUPYTERLAB_DIR=$HOME/.local/share/jupyter/lab

# Bemenu
export BEMENU_BACKEND='wayland'

# Neovim + Mason for LSP
NVIM_MASON_BIN="$HOME/.local/share/nvim/mason/bin"
if [ -d "${NVIM_MASON_BIN}" ]; then
  PATH="${NVIM_MASON_BIN}:${PATH}"
fi

# Nix
# NIX_PATH=
alias nix-search-repl="nix repl --expr 'import <nixpkgs>{}'"
alias nix-search="nix search nixpkgs"
nix-install() {
  nix-env -iA "nixpkgs.$1"
}

# Desktop icons
export XDG_DATA_DIRS="$HOME/.nix-profile/share:$HOME/.var/app/com.valvesoftware.Steam:$XDG_DATA_DIRS"

# Programming Languages
# Perl
if [ "$(cmd_exists perl)" ]; then
  PERL5DIR="$HOME/.local/share/perl5"
  PATH="${PERL5DIR}/bin${PATH:+:${PATH}}";
  PERL5LIB="${PERL5DIR}/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
  PERL_LOCAL_LIB_ROOT="${PERL5DIR}${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
  PERL_MB_OPT="--install_base \"${PERL5DIR}\""; export PERL_MB_OPT;
  PERL_MM_OPT="INSTALL_BASE=${PERL5DIR}"; export PERL_MM_OPT;
fi

# Node.js
if [ "$(cmd_exists npm)" ]; then
  export NPM_PACKAGES=$HOME/.npm-packages
  PATH="$NPM_PACKAGES/bin:$PATH:$HOME/.yarn/bin:"
  # Tell Node about these packages
  export NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH"

  _readable_w3m() {
    readable $@ | w3m -T text/html
  }
  alias readable-w3m="_readable_w3m"
fi

# Java
if [ "$(cmd_exists archlinux-java)" ]; then
  export JAVA_HOME="/usr/lib/jvm/$(archlinux-java get)"
fi
export JAVA_FONTS=/usr/share/fonts/TTF

# Go lang
if [ "$(cmd_exists go)" ]; then
  export GOPATH=$HOME/.go
  export GOBIN=$GOPATH/bin
  PATH="$GOBIN:$PATH"
  alias go-clean="go clean -x -r"
  alias go-build="go build -a -v"
fi

# Rust
. "$HOME/.cargo/env"
alias cg="cargo"
alias cgrun="cargo run --"

# Lua
if [ "$(cmd_exists luarocks)" ]; then
  eval "$(luarocks path --bin --append)"
fi

# Mastodon Toot client
alias toot-public='toot post --editor'
alias toot-unlisted='toot post -v unlisted --editor'
alias toot-private='toot post -v private --editor'

# Snakemake
alias smake="snakemake"

# ssh-askpass
if [ "$(cmd_exists ksshaskpass)" ]; then
  export SSH_ASKPASS="ksshaskpass"
  export GIT_ASKPASS="ksshaskpass"
fi

# ibus
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

# Keyboard layout
# setxkbmap eu

# radicale CardDAV/CalDAV server
export RADICALE_CONFIG=$HOME/.config/radicale/config

# youtube-dl audio download preferences
_youtube_dl_audio() {
  youtube-dl -f bestaudio --continue --no-overwrites --ignore-errors \
    --write-thumbnail --add-metadata --metadata-from-title '(?P<artist>.+?) - (?P<title>.+)' \
    --yes-playlist \
    --extract-audio --audio-format opus --audio-quality 0 \
    -o '%(title)s.%(ext)s' \
    "$@"
}
alias youtube-dl-audio="_youtube_dl_audio"

# Konsole
alias konsole-dark="konsoleprofile -colors=GruvboxDark"
alias konsole-light="konsoleprofile -colors=GruvboxLight"

# pyenv:
# FIXME: slow 0.3s
# export PYENV_ROOT="$HOME/.pyenv"
# if [ -d "$PYENV_ROOT" ] && [ -n "$ZSH_VERSION" ]; then
if [ -d "$PYENV_ROOT" ]; then
  export PATH="${PYENV_ROOT}/bin:${PATH}"
  # https://github.com/pyenv/pyenv/issues/784
  # In zprezto it would be activated using ~/.zprezto/modules/python/init.zsh
  #eval "$(pyenv init --path --no-rehash)"
  eval "$(pyenv init - --no-rehash)"

  # https://github.com/pyenv/pyenv-virtualenv/issues/259
  export PYENV_VIRTUALENV_DISABLE_PROMPT=1
  eval "$(pyenv virtualenv-init - | head -n -3)"

  if [ "$BASH_VERSION" ]; then
    # shellcheck source=~/.pyenv/completions/pyenv.bash
    . "$PYENV_ROOT/completions/pyenv.bash"
    elif [ "$ZSH_VERSION" ]; then
    # shellcheck source=~/.pyenv/completions/pyenv.zsh
    . "$PYENV_ROOT/completions/pyenv.zsh"
  fi

  pyenv-export-virtual-env() {
    local pyenv_version
    pyenv_version="$(pyenv version-name)"
    [ -z "$pyenv_version" ] && echo "pyenv_version undefined" && return
    local prefix
    prefix="$(pyenv virtualenv-prefix)"
    [ -z "$prefix" ] && echo "Not inside a pyenv virtualenv" && return
    export VIRTUAL_ENV="$prefix/envs/$pyenv_version"
  }
fi

# Blog
alias blog='$PYENV_ROOT/versions/www/bin/python -m pelican_ashwinvis.util.write'

# Virtualenv activation and deactivation
ve() {
  _venv_name=${1:-"venv"}
  [ "$_venv_name" = "-h" ] && echo "usage: ve [name]" && return
  if [ ! -d "$_venv_name" ]; then
    python -m venv "$_venv_name"
  fi
  . "$_venv_name/bin/activate"
  unset _venv_name
  hash -r
}
alias vd="deactivate"

# ytfzf
export YTFZF_CONFIG_DIR=$HOME/.config/ytfzf
export YTFZF_CONFIG_FILE=$YTFZF_CONFIG_DIR/conf.sh

# Gaming / AntiMicroX
# [ -f /etc/profile.d/antimicrox_game_controller_mapping.sh ] && . /etc/profile.d/antimicrox_game_controller_mapping.sh

export PATH
USER_SHELL_PROFILE_INIT=1

# Misc
alias wezterm="flatpak run org.wezfurlong.wezterm"
alias fzf-preview="fzf --preview='bat -f {}'"
