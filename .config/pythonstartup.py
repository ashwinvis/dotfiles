from __future__ import print_function
import atexit
import os
import sys


def _colorize(*args, **kwargs):
    """Wrap the args with a specified terminal font color."""
    color_dict = dict(
        HEADER="\033[95m",
        OKBLUE="\033[94m",
        OKGREEN="\033[92m",
        WARNING="\033[93m",
        FAIL="\033[91m",
        ENDC="\033[0m",
    )
    try:
        color = kwargs.pop("color")
        color = color_dict[color]

        if len(args) > 0:
            args = list(args)
            args[0] = "{}{}".format(color, args[0])
            args[-1] = "{}{}".format(args[-1], color_dict["ENDC"])
    finally:
        return args, kwargs


def cprint(*args, **kwargs):
    args, kwargs = _colorize(*args, **kwargs)
    print(*args, **kwargs)

# Custom prompt
# sys.ps1 = ('\x1b[92m' '>>> ' '\x1b[0m')


try:
    __IPYTHON__
except NameError:
    # Not in IPython
    try:
        from rich import pretty, traceback
        pretty.install()
        traceback.install()
    except ImportError:
        cprint("Rich is not installed. No pretty colours.", color="FAIL")
    else:
        del pretty, traceback

    # Tab completion and history
    try:
        import readline
    except ImportError:
        cprint("Readline is not installed. No tab completion is enabled.", color="FAIL")
    else:
        try:
            from jedi.utils import setup_readline

            setup_readline()
            cprint("Jedi is installed", color="OKBLUE")
        except ImportError:
            # Fallback to the stdlib readline completer if it is installed.
            import rlcompleter

            # Taken from ahttp://docs.python.org/2/library/rlcompleter.html
            cprint("Jedi is not installed, falling back to readline", color="WARNING")
            readline.parse_and_bind("tab:complete")

        # Getting history
        # https://docs.python.org/3/library/readline.html
        # Note: Python 2 and PyPy3 doesn't have FileNotFoundError, append_history_file etc.
        HISTFILE = os.path.join(os.path.expanduser("~"), ".python_history")

        try:
            readline.read_history_file(HISTFILE)
            H_LEN = readline.get_current_history_length()
        except IOError:
            open(HISTFILE, "wb").close()
            H_LEN = 0

        def _save_histfile(prev_h_len, histfile):
            # import again
            import readline

            new_h_len = readline.get_current_history_length()
            readline.set_history_length(1000)
            try:
                readline.append_history_file(new_h_len - prev_h_len, histfile)
            except AttributeError:
                pass

        atexit.register(_save_histfile, H_LEN, HISTFILE)
finally:
    # Cleanup
    cprint("Interpreter:", sys.executable, color="OKBLUE")
    del os, sys, atexit  # , readline
    del _colorize, cprint
    pass
