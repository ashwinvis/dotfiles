# Setting up

To generate the SSL certificates you need the `openssl` tool. Then use the bash
scripts inside the SSL directory.

To create a password store, install either:

- Apache and use [htpasswd][htpasswd] utility, preferably using bcrypt
- https://codeberg.org/ashwinvis/radicale-sh

[htpasswd]: https://radicale.org/3.0.html#tutorials/basic-configuration/authentication
