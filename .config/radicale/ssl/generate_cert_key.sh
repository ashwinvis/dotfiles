#!/bin/bash
prefix=${1:-client}

openssl req -x509 -newkey rsa:4096 -keyout ${prefix}_key.pem -out ${prefix}_cert.pem -nodes -days 9999
