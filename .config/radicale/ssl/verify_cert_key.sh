#!/bin/bash
prefix=${1:-client}

# verify: https://kb.wisc.edu/iam/page.php?id=4064
unique_hashes=$(
  (
      openssl x509 -noout -modulus -in ${prefix}_cert.pem | openssl sha1;
      openssl rsa -noout -modulus -in ${prefix}_key.pem | openssl sha1
  ) | uniq
)

f001=$(openssl x509 -in ${prefix}_cert.pem -noout -sha1 -fingerprint)
f256=$(openssl x509 -in ${prefix}_cert.pem -noout -sha256 -fingerprint)

if [[ $(printf "${unique_hashes}" | wc -l) -ne 0 ]]; then
  echo "Verification failed: certificate and key does not match!"
  echo ${unique_hashes}
else
  echo "Verification success!"
  echo "Fingerprints"
  echo "${f001}"
  echo "${f256}"
  echo "${unique_hashes}"
fi
