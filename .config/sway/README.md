# How to

Modify `/usr/share/wayland-sessions/sway.desktop` and replace:

    sway --my-next-gpu-wont-be-nvidia

# Requirements

    ttf-font-awesome
    bemenu-wlroots
    mako
    swaybg
    waybar
    a network-manager-applet-indicator asda

## Optional?

    wallutils
    swayidle
    swaylock
    wofi
    [KDE Wallet](file:///usr/share/doc/arch-wiki/html/en/KDE_Wallet.html)

# Cheatsheet

Alternative to `xprop`:

    swaymsg -t get_tree | grep -i class

Finding out the identifier of touchpad etc:

    swaymsg -t get_inputs

Alternative to `xrandr`:

    swaymsg -t get_outputs


