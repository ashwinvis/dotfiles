# vimrc

Vim / Neovim configuration file with focus on Python, LaTeX, Markdown usage

## Installation

```sh
git clone https://source.coderefinery.org/ashwinvis/dotfiles.git -b subtree/vimrc ~/.config/vimrc
~/.config/vimrc/install.sh
```

