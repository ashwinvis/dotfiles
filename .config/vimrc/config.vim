let g:has_nvim = (has('nvim-0.2') || has('nvim'))
let g:has_vim8 = has('patch-8.0.0039')
let g:has_vim8_1 = has('patch-8.1.0360')
let g:has_python = (has('python') || has('python3'))
