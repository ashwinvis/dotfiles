"colorscheme solarized8
let $VIM_COLOR="desert"
execute('source ~/.config/vimrc/config.vim')
execute('source ~/.config/vimrc/pure.vim')
syntax on
filetype plugin on

" Load the following plugins
for plugin in ["vim-pandoc", "vim-pandoc-syntax"]
  exec 'set runtimepath+=~/.vim/plugged/'.plugin
endfor
" set runtimepath+=~/.vim/plugged/material.vim/
"
