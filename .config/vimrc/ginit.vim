if exists('g:GtkGuiLoaded') || exists('g:GuiLoaded')
    " colorscheme solarized8
    set nu
endif
" Unsuppported command nvim-gtk:
" Guifont DejaVu Sans Mono:h8

" Show filename in window title
set title

" Enable Mouse
set mouse=a

" Set Editor Font
if exists(':GuiFont')
    " Use GuiFont! to ignore font errors
    Guifont! monospace:h12
endif

" Disable GUI Tabline
if exists(':GuiTabline')
    GuiTabline 0
endif

" Disable GUI Popupmenu
if exists(':GuiPopupmenu')
    GuiPopupmenu 0
endif

" Enable GUI ScrollBar
if exists(':GuiScrollBar')
    GuiScrollBar 1
endif

" Enable GUI Treeview
if exists(':GuiTreeviewShow')
    GuiTreeviewShow
endif


" Right Click Context Menu (Copy-Cut-Paste)
nnoremap <silent><RightMouse> :call GuiShowContextMenu()<CR>
inoremap <silent><RightMouse> <Esc>:call GuiShowContextMenu()<CR>
vnoremap <silent><RightMouse> :call GuiShowContextMenu()<CR>gv


