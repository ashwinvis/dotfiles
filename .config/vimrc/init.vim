"author:Ashwin Vishnu M, 2016
"----------------------------
"Requirements:Vim-plug
"Pathogen/Vundle

execute('source ~/.config/vimrc/config.vim')

"{{{Lightline
set laststatus=2
let g:lightline = {
  \ 'colorscheme': 'material',
  \ 'active': {
  \   'left': [ [ 'mode', 'paste' ],
  \             [ 'git', 'readonly', 'filename', 'modified' ] ]
  \ },
  \ 'component_function': {
  \   'git': 'fugitive#head'
  \ },
  \ }
"}}}

"{{{ Polyglot
"Disable LatexBox because we use vimtex
let g:polyglot_disabled = ['latex', 'haproxy']
" }}}

" {{{ Plugin Manager
""""""""""""""""""""
if (g:has_nvim || g:has_vim8) && ! $VIM_PLUG
  "echo "WARNING: vim pack is disabled. Set $VIM_PLUG env var"
  let s:my_plug_config='~/.config/vimrc/packrc.vim'
else
  let s:my_plug_config='~/.config/vimrc/plugrc.vim'
endif
if exists('s:my_plug_config')
  execute('source '.s:my_plug_config)
  command! -nargs=0 -bar PlugConfig execute('edit '.s:my_plug_config)
endif
" }}}

execute('source ~/.config/vimrc/pure.vim')

"{{{ Surround
let g:surround_insert_tail = "<++>"
"}}}

"{{{ FileType and Buffer settings
"""""""""""""""""""""""""""""""""
" au BufNewFile,BufRead *.tex,*.md,*.rst set spell spelllang=en_gb autowrite tw=79
au BufNewFile,BufRead *.cls,*.bbx,*.cbx set filetype=tex
au BufNewFile,BufRead *.code-workspace set filetype=json
if exists(":Pandoc") != 0
  augroup pandoc_syntax
    au! BufNewFile,BufFilePre,BufRead *.pandoc,*.md set filetype=markdown.pandoc
  augroup END
endif
au FileType markdown,pandoc,rst,text,wiki,gitcommit setlocal spell spelllang=en_gb autoread autowrite complete+=kspell

" Use: ~/.editorconfig
" if !exists('g:EditorConfig_verbose')
"   au FileType * setlocal expandtab shiftwidth=2 softtabstop=2 tw=79
"   au FileType make setlocal noexpandtab shiftwidth=8 softtabstop=0
"   au FileType python,tex setlocal shiftwidth=4 softtabstop=4
" endif
au FileType requirements setlocal nospell
" au FileType markdown.pandoc fo+=tacqw "autoformat
if exists('NestedMarkdownFolds')
  au FileType markdown.pandoc setlocal foldexpr=NestedMarkdownFolds()
endif

au BufNewFile,BufRead Snakefile setlocal filetype=snakemake syntax=snakemake
au BufNewFile,BufRead *.smk setlocal filetype=snakemake syntax=snakemake

au BufNewFile,BufRead fonts.conf setlocal filetype=xml syntax=xml
" }}}

"{{{Ultisnips
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
"}}}

"{{{ Python settings
filetype indent on
highlight BadWhitespace ctermbg=darkgreen guibg=lightgreen
au FileType python match BadWhitespace /\s\+$/

" vim-polyglot handles this but we override it with quickfix.py
" au FileType python setlocal makeprg=quickfix.py\ %
" au FileType python setlocal errorformat=%E\"%f\":%l:%m,

au BufNewFile,BufRead *.pyx,*.ipy setlocal filetype=python
let $PYTHONUNBUFFERED=1
au FileType python nmap <buffer> <F8> :w <bar> AsyncRun -raw python %<CR>
au FileType python nmap <buffer> <F9> :exec '!python' shellescape(@%, 1)<CR>
"}}}

"{{{ Pydocstring
nnoremap <silent> <A-d> <Plug>(pydocstring)
let g:pydocstring_formatter = "numpy"
" let g:pydocstring_templates_dir = expand('<sfile>:p:h').'/.vim/plugged/vim-pydocstring/test/templates/numpy/'
let g:pydocstring_doq_path = $HOME . "/.local/bin/doq"
"}}}

"{{{ Neoformat: Black, Isort, Latexindent
"Requirements: pipx install black
let g:black_linelength=82
let g:black_virtualenv='~/.local/pipx/venvs/black'

let g:neoformat_python_black = {
      \ 'exe': 'black',
      \ 'args': ['-l '.g:black_linelength, '-q', '-'],
      \ 'stdin': 1
      \ }

let g:neoformat_python_isort = {
      \ 'exe': 'isort',
      \ 'args': ['-w '.g:black_linelength, '-m 3', '-tc', '-ac', '-q', '-'],
      \ 'stdin': 1
      \ }

let g:neoformat_enabled_python = ['black', 'isort', 'docformatter', 'yapf', 'autopep8']
if !exists(":Black")
  "command Black execute('!black -l '.g:black_linelength.' %:p')
  command Black :Neoformat! python black
endif
if !exists(":Isort")
  " command Isort execute('!isort -w '.g:black_linelength.' -m 3 -tc -ac  %:p')
  command Isort :Neoformat! python isort
endif

let g:neoformat_tex_latexindent = {
        \ 'exe': 'latexindent',
        \ 'args': ['-l', '-g /dev/null'],
        \ 'stdin': 1,
        \ }

let g:neoformat_snakemake_snakefmt = {
        \ 'exe': 'snakefmt',
        \ 'args': ['-l '.g:black_linelength, '-'],
        \ 'stdin': 1,
        \ }

let g:neoformat_enabled_snakemake = ['snakefmt']

"}}}

"{{{ ALE settings
"navigate between errors
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

" Check Python files with flake8 and pylint.
let b:ale_linters = ['flake8', 'vale', 'lacheck', 'chktex', 'shellcheck',
      \ 'eslint', 'jsonlint', 'gcc', 'rustc', 'gawk', 'stack_ghc',
      \ 'csslint']
let g:ale_fixers = {
      \ 'python': ['isort', 'black'],
      \ 'markdown': ['prettier', 'textlint'],
      \ 'markdown.pandoc': ['prettier'],
      \ 'javascript': ['eslint']
      \ }
let g:ale_sign_column_always = 1
let g:ale_lint_delay = 500
"" triggering completion manually <C-x><C-o>
" let g:ale_completion_enabled = 1
" set omnifunc=ale#completion#OmniFunc
"}}}

"{{{Language server
"""""""""""""""""""

" let g:LanguageClient_serverCommands = {
"     \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
"     \ 'javascript': ['/usr/local/bin/javascript-typescript-stdio'],
"     \ 'javascript.jsx': ['tcp://127.0.0.1:2089'],
"     \ 'python': ['/usr/bin/pyls'],
"     \ 'ruby': ['~/.rbenv/shims/solargraph', 'stdio'],
"     \ }
"
" let g:LanguageClient_autoStart = 0
" nnoremap <leader><l> :call LanguageClient_contextMenu()<CR>

let g:lsp_diagnostics_echo_cursor = 1
" if executable('clangd')
"     au User lsp_setup call lsp#register_server({
"         \ 'name': 'clangd',
"         \ 'cmd': {server_info->['clangd', '-background-index']},
"         \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp'],
"         \ })
" endif
"
" if executable('fortls')
"     " pip install fortran-language-server
"     au User lsp_setup call lsp#register_server({
"         \ 'name': 'fortls',
"         \ 'cmd': {server_info->['fortls', '--symbol_skip_mem', '--incrmental_sync', '--autocomplete_no_prefix']},
"         \ 'whitelist': ['fortran'],
"         \ })
" endif
"
" if executable('pyls')
"     " pip install python-language-server
"     au User lsp_setup call lsp#register_server({
"         \ 'name': 'pyls',
"         \ 'cmd': {server_info->['pyls']},
"         \ 'whitelist': ['python'],
"         \ })
" endif

if executable('pylsp') && executable('flake8')
  let g:lsp_settings = {
  \   'pylsp': {
  \       'workspace_config': {'pylsp': {
  \            'configurationSources': ['flake8']}
  \       }
  \   },
  \ }
endif

function! s:on_lsp_buffer_enabled() abort
    " Deoplete?
    setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> <f2> <plug>(lsp-rename)
endfunction

augroup lsp_install
    au!
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

function! s:lsp_disable()
  call lsp#disable()
  let g:lsp_diagnostics_enabled = 0
  let g:lsp_highlights_enabled = 0
  let g:lsp_textprop_enabled = 0
  let g:lsp_virtual_text_enabled = 0
  let g:lsp_highlight_references_enabled = 0
endfunction

command LspDisable :call s:lsp_disable()
"}}}

"{{{ Jedi-Vim settings
autocmd FileType python setlocal completeopt-=preview
let g:jedi#auto_initialization = 0
let g:jedi#auto_vim_configuration = 0
let g:jedi#popup_on_dot = 0
let g:jedi#show_call_signatures = "2"
" let g:jedi#goto_command = "<leader>d"
" let g:jedi#goto_assignments_command = "<leader>g"
" let g:jedi#goto_definitions_command = ""
let g:jedi#documentation_command = "<leader>d"
" let g:jedi#usages_command = "<leader>n"
" let g:jedi#completions_command = "<C-Space>"
" let g:jedi#rename_command = "<leader>r"
let g:jedi#completions_enabled = 0  "Deoplete-jedi for completions
let g:pymode_rope = 0
"}}}

"{{{ Deoplete
if(g:has_python && has('g:deoplete'))
  let g:deoplete#enable_at_startup = 1
  let g:deoplete#sources#jedi#ignore_errors = v:true
  call deoplete#custom#option('sources', {
  \ '_': ['ale'],
  \ })
  call deoplete#custom#option({
  \ 'refresh_always': v:false,
  \ })
" let g:deoplete#auto_refresh_delay = 1000
" let g:deoplete#refresh_always = v:false
" let g:deoplete#max_list = 4
" call deoplete#custom#var('omni', 'input_patterns', {
"   \ 'pandoc': '@'
"   \})
endif
"}}}

"{{{ NERD Commenter
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1
" Align line-wise comment delimiters flush left instead of following code
" indentation
let g:NERDDefaultAlign = 'left'
" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1
" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
"}}}

"{{{ LaTeX settings: synctex, vimtex

"vimtex
let g:tex_flavor = "latex"
" let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0

"tex-conceal
let g:tex_conceal='abdmg'

function! SyncTexForward()
     "let execstr = "silent !okular --unique %:p:r.pdf\\#src:".line(".")."%:p &"
     let execstr = "silent !zathura --synctex-forward ".line(".").":".col(".").":%:p %:p:r.pdf &"
     exec execstr
endfunction
"au FileType tex nmap <buffer> <F9> :exec 'silent !make'<CR>
" au FileType tex nmap <buffer> <F8> :w <bar> AsyncRun latexmk -synctex=1 -interaction=nonstopmode -file-line-error -pdf -pdflatex=pdflatex -shell-escape %<CR>
au FileType tex nmap <buffer> <F8> :w <bar> AsyncRun make %<CR>
au FileType tex nmap <buffer> <F9> :call SyncTexForward()<CR>

let g:vimtex_compiler_progname='nvr'
"}}}

"{{{Goyo, Limelight, Startify
"""""
function! s:current_colorscheme()
  redir => output
  silent colorscheme
  redir END
  return output
endfunction

let g:goyo_width = 90
let g:goyo_height = 99
if empty($VIM_COLOR)
  let $GOYO_COLOR = "solarized8_low"
else
  let $GOYO_COLOR = $VIM_COLOR
endif

function! s:goyo_enter() "color_new, color_old)
  silent !tmux set status off
  silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
  set noshowmode
  set noshowcmd
  set scrolloff=999
  if exists(":Limelight")
    Limelight
  endif

  colorscheme $GOYO_COLOR
  " ...
endfunction

function! s:goyo_leave() "color_old)
  silent !tmux set status on
  silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
  set showmode
  set showcmd
  set scrolloff=5
  if exists(":Limelight")
    Limelight!
  endif
  colorscheme $VIM_COLOR
  call g:SetBackgroundByTime()
  " ...
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter() "g:goyo_color, g:goyo_color_old)
autocmd! User GoyoLeave nested call <SID>goyo_leave() "g:goyo_color_old)

"}}}

"{{{Delimitmate
""""""""""""
"Alternate jump cursor mapping
imap <C-k> <Plug>delimitMateJumpMany
let delimitMate_expand_cr = 1
" If you have <CR> expansion enabled, you might want to skip it on pop-up
" menus:
imap <expr> <CR> pumvisible()
                 \ ? "\<C-Y>"
                 \ : "<Plug>delimitMateCR"
let delimitMate_jump_expansion = 1
"}}}

"{{{ Mundo
""""""
noremap <F5> :MundoToggle<CR>
let g:mundo_prefer_python3 = 1
"}}}

"{{{ Markdown / Pandoc: vim-instant-markdown, vim-pandoc, markdown-preview.nvim
" au FileType markdown setlocal makeprg=vale\ --output=line\ %a

" Disable by default to avoid opening instant-markdown on new files in the
" InstantMarkdownGroup blocklist
" FIXME: Help wanted in fixing this. BufNewFile doesn't work
let g:instant_markdown_autostart=0

augroup InstantMarkdownGroup
  " Remove ALL autocommands for the current group.
  autocmd!
  " blocklist
  au! BufReadPre,BufNewFile,BufEnter,FileReadCmd,BufFilePre *.md let g:instant_markdown_autostart=0
  " allowlist
  au! BufReadPre,BufNewFile,BufEnter,FileReadCmd,BufFilePre ~/src/articles/*.md,~/www/website/*.md let g:instant_markdown_autostart=1
augroup END

let g:instant_markdown_mathjax = 1
let g:instant_markdown_mermaid = 1
if executable('qutebrowser')
  let g:instant_markdown_browser = 'qutebrowser'
endif
let g:instant_markdown_slow = 0
let g:instant_markdown_python = 0
let g:instant_markdown_logfile = '/tmp/instant_markdown.log'

let vim_markdown_preview_hotkey='<F10>'
let vim_markdown_preview_browser='Firefox'
let vim_markdown_preview_use_xdg_open=1
let vim_markdown_preview_pandoc=1
let vim_markdown_preview_toggle=2

" hi clear Conceal  " for vim-pandoc-syntax
let g:pandoc#formatting#mode='hA'
let g:pandoc#formatting#textwidth=79
let g:pandoc#biblio#sources='c'
" let g:pandoc#biblio#bibs='thesis.bib'
" let g:pandoc#filetypes#handled = ['pandoc', 'markdown', 'vimwiki']

au BufReadPost *.md set syntax=markdown.pandoc ft=markdown.pandoc

" plasticboy/vim-markdown
let g:vim_markdown_math=1

au FileType pandoc nmap <buffer> <F8> :w <bar> AsyncRun make "%:r".pandoc.pdf<CR>

let g:mkdp_auto_start = 1
let g:mkdp_open_ip = 'localhost:8998'
let g:mkdp_browser = 'xdg-open firefox'
"}}}

"{{{ CtrlP.vim
"Search files, buffers and MRU
let g:ctrlp_cmd = 'CtrlPMixed'
"}}}

"{{{ Airline
""""""""
let g:airline_powerline_fonts = 1
let g:airline#extensions#ale#enabled = 1
"}}}

"{{{ LanguageTool
let g:languagetool_cmd='/usr/bin/languagetool'
"let g:languagetool_jar='/usr/share/java/languagetool/languagetool-commandline.jar'
"let g:languagetool_lang='en-GB'
"}}}

"{{{ FZF
function! Bibtex_ls()
  let bibfiles = (
      \ globpath('.', '*.bib', v:true, v:true) +
      \ globpath('..', '*.bib', v:true, v:true) +
      \ globpath('*/', '*.bib', v:true, v:true)
      \ )
  let bibfiles = join(bibfiles, ' ')
  let source_cmd = 'bibtex-ls '.bibfiles
  return source_cmd
endfunction

function! s:bibtex_cite_sink_insert(lines)
    let r=system("bibtex-cite ", a:lines)
    execute ':normal! i' . r
    call feedkeys('a', 'n')
endfunction

inoremap <silent> @@ <c-g>u<c-o>:call fzf#run({
                        \ 'source': Bibtex_ls(),
                        \ 'sink*': function('<sid>bibtex_cite_sink_insert'),
                        \ 'up': '40%',
                        \ 'options': '--ansi --layout=reverse-list --multi --prompt "Cite> "'})<CR>
"}}}

" {{{Vimwiki
"enable mouse
let g:vimwiki_use_mouse = 1
let wiki_1 = {}
let wiki_1.auto_toc = 1
let wiki_1.path = '~/Wiki/src'
let wiki_1.path_html = '~/Wiki'
let wiki_1.template_path = '~/Wiki/assets/'
let wiki_1.template_default = 'default'
let wiki_1.template_ext = '.tpl'

let wiki_2 = {}
let wiki_2.path = '~/Wiki/md/src'
let wiki_2.path_html = '~/Wiki/md'
" let wiki_2.custom_wiki2html=$HOME.'/.vim/plugged/vimwiki/autoload/vimwiki/customwiki2html.sh'
let wiki_2.custom_wiki2html='~/Wiki/md2html.py'
let wiki_2.syntax = 'markdown'
let wiki_2.ext = '.md'
" let wiki_2.html_template = '~/src/www/ashwinvis.github.io/src/wiki_template.tpl'
" let wiki_2.nested_syntaxes = {'python': 'python', 'c++': 'cpp'}

let g:vimwiki_list = [wiki_1, wiki_2]

"Vimwiki considers every markdown-file as a wiki file
let g:vimwiki_global_ext = 0

"lervag/wiki.vim
let g:wiki_root = '~/Wiki/src'
let g:wiki_journal = {'name': 'diary'}

"}}}

" {{{Rust
" makeprg
au FileType rust setlocal makeprg=cargo\ run

" }}}

"{{{vim-test
let test#strategy = {
  \ 'nearest': 'neovim',
  \ 'file':    'asyncrun_background_term',
  \ 'suite':   'basic',
\}
"}}}

" {{{xonsh
au Filetype xonsh setlocal comments=b:#,fb:- commentstring=#\ %s
" }}}

"{{{ Custom commands: AVFocusMode
function! s:focus_mode()
  LspDisable
  ALEDisable
  SignifyDisable
  " :call s:goyo_enter()
endfunction
command! AVFocusMode :call s:focus_mode()
"}}}

if exists('s:my_plug_config') && match(s:my_plug_config, '.*packrc.*') == 0
  call PackPostVimrc()
endif
