#!/usr/bin/env bash
set -xueo pipefail
declare -r YADM_WORK=$(yadm rev-parse --show-toplevel)


cd $YADM_WORK/.config/vimrc

#Vim
if [ -f $YADM_WORK/.vimrc ]; then
  echo "Backing up Vim configuration $YADM_WORK/.vimrc -> $YADM_WORK/.vimrc.bak"
  mv -f $YADM_WORK/.vimrc $YADM_WORK/.vimrc.bak
fi

ln -s "$PWD"/init.vim $YADM_WORK/.vimrc

#Neovim
if [ -d $YADM_WORK/.config/nvim ]; then
  echo "Backing up Neovim configuration $YADM_WORK/.config/nvim -> $YADM_WORK/.config/nvim.bak"
  mv -f $YADM_WORK/.config/nvim $YADM_WORK/.config/nvim.bak
fi

mkdir -p $YADM_WORK/.config/nvim/
ln -rsf "$PWD"/nvimrc.vim $YADM_WORK/.config/nvim/init.vim
ln -rsf "$PWD"/ginit.vim $YADM_WORK/.config/nvim/ginit.vim

# Neovim Python
(
  set +u
  pushd "$YADM_WORK/.config/nvim"
  python3 -m venv venv
  source venv/bin/activate
  pip install pynvim wheel
  deactivate
  popd
)

#spell
mkdir -p $YADM_WORK/.config/nvim/spell $YADM_WORK/.vim/spell
ln -rsf $YADM_WORK/.config/vimrc/en.utf-8.add $YADM_WORK/.config/nvim/spell/en.utf-8.add
ln -rsf $YADM_WORK/.config/vimrc/en.utf-8.add $YADM_WORK/.vim/spell/en.utf-8.add

#editorconfig
ln -rsf $YADM_WORK/.config/vimrc/.editorconfig $YADM_WORK/.editorconfig

#ultisnips
curl -fLo $YADM_WORK/.vim/UltiSnips/tex.snippets --create-dirs https://raw.githubusercontent.com/gillescastel/latex-snippets/master/tex.snippets

#pack
mkdir -p $YADM_WORK/.local/share/nvim/site
if [ -d $YADM_WORK/.vim/pack ]; then
  ln -rsf $YADM_WORK/.vim/pack $YADM_WORK/.local/share/nvim/site/pack
fi

#vim-plug
curl -fLo $YADM_WORK/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
curl -fLo $YADM_WORK/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
VIM_PLUG=1 vim +PlugInstall
