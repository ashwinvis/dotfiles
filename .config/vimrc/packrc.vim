"{{{vim-unimpaired keybindings
nmap ]b :bnext<CR>
nmap [b :bprev<CR>
nmap ]B :blast<CR>
nmap [B :bfirst<CR>
nmap ]q :cnext<CR>
nmap [q :cprev<CR>
nmap ]Q :clast<CR>
nmap [Q :cfirst<CR>
nmap ]t :tnext<CR>
nmap [t :tprev<CR>
nmap ]T :tlast<CR>
nmap [T :tfirst<CR>
nmap ]on :set number<CR>
nmap [on :set nonumber<CR>
nmap ]os :set spell<CR>
nmap [os :set nospell<CR>
"}}}

packadd vim-surround delimitmate \
  \ vim-markdown-folding
  \ vim-instant-markdown

au BufNewFile,BufFilePre,BufRead *.pandoc,*.md packadd vim-pandoc-syntax

if g:has_python
  " if g:has_nvim || g:has_vim8
  "   packadd deoplete.nvim
  " endif
  if g:has_nvim
    packadd nvim-lspconfig vim-compe nvim-treesitter playground plenary.nvim telescope.nvim
    "popup.nvim
  endif
  " if g:has_vim8
  "   packadd nvim-yarp
  "   packadd vim-hug-neovim-rpc
  " endif
endif

function! PackPostVimrc()
  " For functions that work better when loaded at end of vimrc
  " packadd vimwiki vim-mundo securemodelines

  " if g:has_python
  "   packadd deoplete-jedi
  "   packadd deoplete-vim-lsp
  " endif

  packloadall
endfunction

