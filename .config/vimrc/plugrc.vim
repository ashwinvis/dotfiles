" To install
" $ curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" $ curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
execute('source ~/.config/vimrc/config.vim')

call plug#begin('~/.vim/plugged')

Plug 'junegunn/vim-plug'

"{{{ Colorschemes
Plug 'lifepillar/vim-solarized8'
"Plug 'altercation/vim-colors-solarized'
"Plug 'chriskempson/base16-vim'
"Plug 'junegunn/seoul256.vim'
Plug 'morhetz/gruvbox'

Plug 'machakann/vim-highlightedyank'

"}}}

"{{{ Generic plugins
"Modelines parser
Plug 'ciaranm/securemodelines'
"Fancy start screen
" Plug 'mhinz/vim-startify'  -> has issues with help, stdin
"Commenter
Plug 'scrooloose/nerdcommenter'
"Editorconfig
Plug 'editorconfig/editorconfig-vim'
"Repeat last mapping instead of last native command
Plug 'tpope/vim-repeat'
"Plug 'tpope/vim-vinegar' "Some shortcuts and enhancements for netrw: Freezes neovim?
"Navigate: with []. q=quickfix, a=current, b=buffer, e=exchangelines, f=files, n=vcs
Plug 'tpope/vim-unimpaired'
"Tab as spaces
Plug 'ervandew/supertab'
"Airline
" Plug 'vim-airline/vim-airline'
" Plug 'vim-airline/vim-airline-themes'
Plug 'itchyny/lightline.vim'
" Plug 'itchyny/vim-gitbranch'
"Auto delimiter quotes, brackets etc
Plug 'raimondi/delimitmate'
"Surround.vim is all about parentheses, brackets, quotes, XML tags, and more:
Plug 'tpope/vim-surround'
"Remember last place
Plug 'farmergreg/vim-lastplace'
"Vim bindings for autojump python CLI tool
Plug 'ashwinvis/autojump', {
  \ 'dir': '~/.local/opt/autojump',
  \ 'do': './install.py  -d ~/.local -z etc/zsh_completion.d'
  \  }
" if g:has_python && executable('autojump')
"   Plug 'trotter/autojump.vim'
" endif
"Fuzzy finder
Plug 'ctrlpvim/ctrlp.vim'
" Plugin outside ~/.vim/plugged with post-update hook
" FZF - another fuzzy finder
if executable('go')
  Plug 'junegunn/fzf', { 'dir': '~/.local/opt/fzf', 'do': './install --bin' }
  Plug 'junegunn/fzf.vim'
endif
"Show Diff
Plug 'mhinz/vim-signify'
"Enhanced syntax highlighting
if ! g:has_nvim
  " In neovim we have treesitter
  Plug 'sheerun/vim-polyglot'
endif
"Autoformat
Plug 'sbdchd/neoformat'
"Snippets
Plug 'sirver/ultisnips'

"xonsh
Plug 'linkinpark342/xonsh-vim'

"BramMool's game
" Plug 'vim/killersheep'

"}}}

"{{{ Python
"Easy docstrings
Plug 'heavenshell/vim-pydocstring', {'for': 'python'}
"Fix indentation issues
Plug 'vim-scripts/indentpython.vim', {'for': 'python'}
"Open and edit Jupyter notebooks using `jupytext` package
Plug 'goerz/jupytext.vim'
"Requirements syntax
Plug 'raimon49/requirements.txt.vim', {'for': 'requirements'}

"IDE-like experience
Plug 'z-huabao/vim-submode', {'for': ['python', 'snakemake', 'xonsh']}
Plug 'z-huabao/vim-slime-ipython', {'for': ['python', 'snakemake', 'xonsh']}
"}}}

"{{{ Distraction free
Plug 'junegunn/goyo.vim', {'for': ['tex', 'text', 'markdown', 'rst'], 'on': 'Goyo'}
Plug 'junegunn/limelight.vim', {'for': ['text', 'markdown', 'rst'], 'on': 'Goyo'}
" }}}

"{{{ Latex & Co
"Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
" Plug 'lervag/vimtex', {'for': ['tex', 'markdown']} "-> slow
Plug 'dpelle/vim-LanguageTool', {'for': ['text', 'markdown', 'rst']}
Plug 'KeitaNakamura/tex-conceal.vim'
Plug 'dhruvasagar/vim-table-mode', {'for': ['markdown.pandoc', 'markdown', 'rst']}
"}}}

"{{{ Markdown
"The tabular plugin must come before vim-markdown
" Plug 'godlygeek/tabular', {'for': ['markdown', 'markdown.pandoc']}
" Plug 'plasticboy/vim-markdown', {'for': ['markdown', 'markdown.pandoc']}
"Plug 'suan/vim-instant-markdown', {'for': 'markdown'}
"Plug 'ashwinvis/vim-instant-markdown', {'for': 'markdown'}
"Plug 'ashwinvis/vim-instant-markdown', {'for': 'markdown', 'dir': '~/src/vim-markdown/vim-instant-markdown'}
let g:ashwins_sources = expand('~/Sources')
if isdirectory(g:ashwins_sources . '/instant-markdown/vim-instant-markdown')
  Plug (g:ashwins_sources . '/instant-markdown/vim-instant-markdown'), {'for': ['markdown', 'markdown.pandoc']}
else
  Plug 'instant-markdown/vim-instant-markdown', {'for': ['markdown', 'markdown.pandoc']}
endif
if isdirectory(g:ashwins_sources . '/boring-writing')
  Plug g:ashwins_sources . '/boring-writing'
else
  Plug 'https://codeberg.org/ashwinvis/boring-writing'
endif
"Plug 'iamcco/markdown-preview.nvim', {'for': 'markdown',  'do': 'cd app & yarn install'}
"Plug 'JamshedVesuna/vim-markdown-preview', {'for': 'markdown'}
" Plug 'vim-pandoc/vim-pandoc', {'on': 'Pandoc'}
Plug 'vim-pandoc/vim-pandoc-syntax', {'for': ['markdown', 'markdown.pandoc']}
Plug 'masukomi/vim-markdown-folding', {'for': ['markdown', 'markdown.pandoc']}
"}}}

"{{{Tasks, notes, wiki

"vimwiki
" Plug 'vimwiki/vimwiki' ", {'branch': 'dev'}
Plug 'lervag/wiki.vim'
Plug 'lervag/wiki-ft.vim'

Plug 'mattn/calendar-vim'

"taskwarrior
"requires taskwarrior (task), git+git://github.com/robgolding63/tasklib@develop
"Plug 'tbabej/taskwiki'
"Plug 'blindFS/vim-taskwarrior'
"}}}

"{{{Rst
Plug 'gu-fan/riv.vim', {'for': 'rst'}
" Plug 'gu-fan/InstantRst', {'for': 'rst'}
"}}}

"{{{JSON
Plug  'elzr/vim-json', {'for': 'json'}
"}}}

"{{{ VCS plugins
Plug 'tpope/vim-fugitive' ", {'tag': 'v2.5'}
" Plug 'ludovicchabant/vim-lawrencium'
"}}}

"{{{ Async plugins
""""""""""""""
if g:has_nvim || g:has_vim8
  Plug 'dense-analysis/ale'
  Plug 'skywind3000/asyncrun.vim'
  Plug 'vim-test/vim-test'
endif
"}}}

"{{{Language server, Tree-sitter, Telescope
""""""""""""""""
"Plug 'autozimu/LanguageClient-neovim', {'branch': 'next', 'do': 'bash install.sh'}

if g:has_nvim
  Plug 'neovim/nvim-lspconfig'
  Plug 'hrsh7th/nvim-compe'

  Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
  Plug 'nvim-treesitter/playground'

  " dependencies
  Plug 'nvim-lua/plenary.nvim'
  Plug 'nvim-lua/popup.nvim'
  " telescope
  Plug 'nvim-telescope/telescope.nvim'

  " colorschemes which support treesitter
  " Written in Lua
  Plug 'folke/tokyonight.nvim', {'branch': 'main'}
  " Plug 'tanvirtin/monokai.nvim'
  Plug 'marko-cerovac/material.nvim', {'branch': 'main'}
  " Written in VimL
  Plug 'sainnhe/sonokai'


else
  Plug 'prabirshrestha/async.vim'
  Plug 'prabirshrestha/vim-lsp'
  Plug 'mattn/vim-lsp-settings'

  " colorscheme: material
  " Plug 'chriskempson/base16-vim'
  " Plug 'kaicataldo/material.vim'
  if isdirectory(expand('~/src/projects/material.vim'))
    Plug '~/src/projects/material.vim'
  else
    Plug 'ashwinvis/material.vim', {'branch': 'dev'}
  endif
  " Plug 'hzchirs/vim-material'
endif

"}}}

"{{{ Plugins which require python
if g:has_python
  "Plug 'sjl/gundo.vim'
  "Undo
  "Plug 'simnalamburt/vim-mundo'
  Plug 'mbbill/undotree'

  " Plug 'davidhalter/jedi-vim', {'for': 'python', 'on': 'Pyimport'}
  " Plug 'deoplete-plugins/deoplete-jedi', {'for': 'python'}
  " Plug 'lighttiger2505/deoplete-vim-lsp'

  " requires: python-bibtexparser
  " Plug 'lionawurscht/deoplete-biblatex', {'for': ['markdown', 'tex']}
  if g:has_nvim
    " Plug 'Shougo/deoplete.nvim', {'do': ':UpdateRemotePlugins'}
  else
    if g:has_vim8
      "Syntax checker / linter
      " Plug 'Shougo/deoplete.nvim'
      "Fuzzy finder
      " Plug 'roxma/nvim-yarp'
      " Plug 'roxma/vim-hug-neovim-rpc'
    endif
  endif

  if g:has_nvim || g:has_vim8
    " MRU plugin includes unite.vim/denite.nvim MRU sources
    " Plug 'Shougo/neomru.vim'
  endif
endif
" }}}

"{{{Vim unittests
Plug 'junegunn/vader.vim'
"}}}

call plug#end()
