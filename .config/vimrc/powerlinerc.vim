"Powerline settings
"""""""""""""""""""
let g:powerline_pycmd = "py3"
py3 from powerline.vim import setup as powerline_setup
py3 powerline_setup()
py3 del powerline_setup
set rtp +=/usr/lib/python3.6/site-packages/powerline/bindings/vim
set laststatus=2
