"No-plugin config, apart from colorschemes


"{{{ Colorscheme settings
"Current line highlight
set cursorline

" For Neovim 0.1.3 and 0.1.4 - https://github.com/neovim/neovim/pull/2198
if (has('nvim'))
  let $NVIM_TUI_ENABLE_TRUE_COLOR = 1
endif

" For Neovim > 0.1.5 and Vim > patch 7.4.1799
if $COLORTERM == "truecolor" && has('termguicolors')
  set termguicolors
else
  let base16colorspace=256  " Access colors present in 256 colorspace
endif

function! g:SetBackgroundByTime()
  if strftime("%H") > 7 && strftime("%H") < 17
    set background=light
    let g:material_theme_style = 'default'
  else
    set background=dark
    " let g:material_theme_style = 'palenight'
    if strftime("%H") > 17 || strftime("%H") < 4
      let g:material_theme_style = 'darker'
    endif
  endif
  "Override
  """"""""
  "set background=dark
  "set background=light
endfunction

if empty($VIM_COLOR)
  if has('gui_running')
      " colorscheme solarized8
      " let $VIM_COLOR="solarized8_flat"
      let $VIM_COLOR="material"
  else
      "let $VIM_COLOR="gruvbox"
      "let $VIM_COLOR="solarized8_low"
      let $VIM_COLOR="material"
      "Enable mouse scrolling and clicking in terminal
      set mouse=a
  endif

  if &diff
      " let $VIM_COLOR="solarized8_high"
      " let g:solarized_visibility="high"  "default value is normal
      " let g:solarized_contrast="high"    "default value is normal
      " let g:solarized_diffmode="high"    "default value is normal

      let $VIM_COLOR="material"
      let g:material_style = 'deep ocean'
  endif

endif

" set light/dark background works best with solarized themes
if (match($VIM_COLOR, '/^solarized.*/') == -1) || ("material" == $VIM_COLOR)
  call g:SetBackgroundByTime()
endif

try
  execute('colorscheme '.$VIM_COLOR)
catch
  set t_Co=256
  try
    execute('colorscheme '.$VIM_COLOR)
  catch
    colorscheme desert
  endtry
endtry

" for material.vim theme
let g:material_terminal_italics = 1
" for vim-material theme
" let g:material_style = g:material_theme_style

"}}}

"{{{ Generic, global, pure vim
set relativenumber
" set splitbelow splitright
set mouse=a
set mouseshape=n:beam,ve:beam,sd:updown
set nocompatible  " vimwiki
syntax on
set hidden " For language server?
set nomodeline  " Disallow exploit, use ciaranm/securemodelines if needed
set conceallevel=1
set backup  "backup files to backupdir
set undofile
if g:has_nvim
  set shada+=n~/.cache/nvim/viminfo
  set backupdir-=.  " Do not save backup files in current directory
  set undodir=~/.cache/nvim/undo  "Undo files are not compatible
else
  set viminfo+=n~/.cache/vim/viminfo
  set undodir=~/.cache/vim/undo
endif

"To avoid issues with shells like xonsh
set shell=/bin/bash

"ensure backupdir
"FIXME: Something wring with this approach. It tries mkdir .,
" let s:backupdir=&backupdir
" if !isdirectory(s:backupdir)
"   silent exec "!mkdir -p ".s:backupdir
" endif

if getcwd() =~# '^\(/home/avmo/src/snek5000\)'
  set secure exrc  "execute project / directory specific .vimrc
endif

cmap w!! w !sudo tee > /dev/null %

filetype plugin indent on
if executable('rg')
  set grepprg=rg\ --vimgrep\ $*
elseif executable('ag')
  set grepprg=ag\ --vimgrep\ $*
elseif executable('ack')
  set grepprg=ack\ --nogroup\ --nocolor\ --column\ $*
else
  set grepprg=grep\ -rnH\ $*
endif
set grepformat+=%f:%l:%c:%m

"diff: patience algorithm
if g:has_vim8_1 || g:has_nvim
  set diffopt+=internal,algorithm:patience
endif

"Syntax highlighting above the cursor
nnoremap <leader>syn :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

"}}}

"{{{ Generic custom shortcuts
"""""""""""""""""""""""""""""
"Avoid fzf.vim from hijacking with the windows command
command! W w


"Jump to end of the line
nnoremap <C-j> f<Space>

"Clipboard
"Now all regular copy operations will copy to the system clipboard too :)
"set clipboard=unnamedplus
"copy
vnoremap <C-c> "+y
"paste
inoremap <C-v> <C-o>"+gp

"Buffer switching with ls
"FZF reassign Not working :(
if exists(':Buffers')
  nnoremap <leader>b :Buffers
else
  nnoremap <leader>b :ls<cr>:b
endif
"Switch buffers
nnoremap <A-b> :b#<cr>
inoremap <A-b> <Esc>:b#<cr>i
"Switch windows
nnoremap <silent> <A-w> <C-w><C-w>
inoremap <silent> <A-w> <Esc><C-w><C-w>i
vnoremap <silent> <A-w> <Esc><C-w><C-w>

"Correct using first spelling suggestion and move to end of the word
nnoremap <leader>s 1z=e
"It basically jumps to the previous spelling mistake [s, then picks the
"first sugges­tion 1z=, and then jumps back `]a. The <c-g>u in the middle
"make it possible to undo the spelling correction quickly
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u

"Duplicate line
nnoremap YY yyp

"Delete till end of line
"inoremap <C-W> <C-o>dw

"Diff the original file: esp. when recovering a swap file
if !exists(':DiffOrig')
  command DiffOrig vert new | set buftype=nofile | read ++edit # | 0d_
    \ | diffthis | wincmd p | diffthis
endif

" FIXME: not working :(
if !exists(':DiffSplit')
  function! DiffFiles(...)
     let i = 0
     while i < argc()
      if i > 0
        execute "vsplit"
      endif
      execute "find " . argv(i). " | diffthis"
      let i = i + 1
     endwhile
  endfunction

  command! -nargs=* -complete=file DiffSplit call DiffFiles(<f-args>)
endif


"}}}

"{{{ Auto save and load sessions
function! MakeSession()
  let b:sessiondir = $HOME . "/.vim/sessions" . getcwd()
  if (filewritable(b:sessiondir) != 2)
    exe 'silent !mkdir -p ' b:sessiondir
    redraw!
  endif
  let b:filename = b:sessiondir . '/session.vim'
  exe "mksession! " . b:filename
endfunction

" Updates a session, BUT ONLY IF IT ALREADY EXISTS
function! UpdateSession()
  let b:sessiondir = $HOME . "/.vim_session" . getcwd()
  let b:sessionfile = b:sessiondir . "session.vim"
  if (filereadable(b:sessionfile))
    exe "mksession! " . b:filename
  endif
endfunction

function! LoadSession()
  let b:sessiondir = $HOME . "/.vim/sessions" . getcwd()
  let b:sessionfile = b:sessiondir . "/session.vim"
  if (filereadable(b:sessionfile))
    exe 'source ' b:sessionfile
  else
    echo "No session loaded."
  endif
endfunction

function! DelSession()
  exe 'bufdo! bdel'
  call MakeSession()
endfunction

if(argc() == 0)
  au VimEnter * nested :call LoadSession()
endif

nnoremap <leader>ms :call MakeSession()<CR>
nnoremap <leader>ds :call DelSession()<CR>
""}}}

"{{{Ctags
"Accept the tags file as a hidden file
set tags^=.tags

"Generate tags from specified path, virtual env / conda prefix
function! s:make_ctags(command)
  if argc() == 0
    let l:path = ''
  else
    let l:path = argv(0)
  endif

  echo system('ctags -f .tags -R '.l:path)

  for env_var in ['VIRTUAL_ENV', 'CONDA_PREFIX']
    let l:py_prefix = getenv(env_var)
    if l:py_prefix != v:null
      echo system('ctags -f .tags --append --languages=Python -R '.l:py_prefix.'/lib')
    endif
  endfor
endfunction

command! -nargs=? -complete=file Ctags call s:make_ctags(<q-args>)
"}}}

"""{{{Project automation: Ctags, set path (only executed for git and hg repos)
if isdirectory('.git') || isdirectory('.hg')
  if !filereadable(".tags")
    Ctags .
  endif

  set path+=**
endif
"""}}}

"{{{ NERD Tree / Netrw
"""""""""""""""""""
" map <C-n> :NERDTreeToggle<CR>
map <C-n> :Lexplore<CR>
let g:netrw_winsize = 21
let g:netrw_banner = 1
let g:netrw_liststyle = 3
let g:netrw_browse_split = 2
let g:netrw_altv = 1
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'

" Autostart netrw
" augroup ProjectDrawer
"   autocmd!
"   autocmd VimEnter * :Lexplore
" augroup END

"}}}

"{{{ IDE mode (<leader>t) and Terminal (<Alt-T>)

" Terminal Function
let g:main_win = 0
let g:term_buf = 0
let g:term_win = 0
let g:term_height = 12

function! TermToggle(height)
    if win_gotoid(g:term_win)
        hide
    else
        let g:main_win = win_getid()
        botright new
        exec "resize " . a:height
        try
            exec "buffer " . g:term_buf
        catch
            call termopen($SHELL, {"detach": 0})
            let g:term_buf = bufnr("")
            set nonumber
            set norelativenumber
            set signcolumn=no
        endtry
        startinsert!
        let g:term_win = win_getid()
    endif
endfunction

function! IDEMode(height)
  call TermToggle(a:height)
  Lexplore
  if win_gotoid(g:main_win)
  endif
endfunction

" Toggle terminal on/off (neovim)
nnoremap <A-t> :call TermToggle(g:term_height)<CR>
inoremap <A-t> <Esc>:call TermToggle(g:term_height)<CR>

" Toggle IDE mode on/off
nnoremap <leader>t :call IDEMode(g:term_height)<CR>

if g:has_nvim || g:has_vim8
  tnoremap <leader>t <C-\><C-n>:call IDEMode(g:term_height)<CR>
  tnoremap <A-t> <C-\><C-n>:call TermToggle(g:term_height)<CR>

  tnoremap <ESC> <C-\><C-n>
  tnoremap :q! <C-\><C-n>:q!<CR>
endif
"}}}

"{{{Fuzzy finder to search subdirectories using `find` command
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Disable because we use Deoplete
" set path+=**
set wildmenu
"set wildmode=list:full
"}}}

"{{{ Folding
"""""""""""""""
set foldlevelstart=99
set foldlevel=99
au FileType vim setlocal foldmethod=marker
" au FileType fortran setlocal foldmethod=marker foldmarker=c-,end
let fortran_fold=1
au FileType fortran setlocal foldmethod=syntax
au FileType json,c,cpp,xml,html,xhtml setlocal foldmethod=syntax
au FileType python setlocal foldmethod=indent
" All folds closed
au FileType vim,markdown normal zM
"All folds open
au FileType json,c,cpp,fortran,xml,html,xhtml,perl normal zR

nnoremap <space> za
"}}}

"{{{Redirect vim command output
function! s:redirect_output(cmd)
  redir! > /tmp/vim_output.log
  silent verbose cmd
  redir END
  edit /tmp/vim_output.log
endfunction

"FIXME: should work like :RedirectOutput nmap to see all the available mappings
command! -nargs=1 -complete=command RedirectOutput call s:redirect_output(<args>)
"""}}}

"{{{ Word Count
command! WordCount execute('w !detex | wc -w')
"}}}
