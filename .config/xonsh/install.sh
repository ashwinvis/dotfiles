#!/bin/bash
set -e
PYTHON_VERSION=${PYTHON_VERSION:-3.8.9}
pipx reinstall --python $HOME/.pyenv/versions/$PYTHON_VERSION/bin/python xonsh
pipx runpip xonsh install -r requirements.txt
