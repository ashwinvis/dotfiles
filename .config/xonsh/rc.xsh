# vim: set syntax=python
import os
import shutil
from contextlib import suppress
from functools import lru_cache
from socket import gethostname
from pathlib import Path

from xonsh import xontribs
import xontrib.voxapi as voxapi

def _is_xontrib_installed(xpkg):
    parser = xontribs._create_xontrib_parser()
    ns = parser.parse_args(["list", "--json", xpkg])
    data = xontribs.xontrib_data(ns)
    print(data)
    # return bool(
    #     [pkg for pkg in xontribs.xontrib_metadata()['xontribs'] if pkg['name']==xpkg]
    # )

# XONSH WIZARD START
$AUTO_CD = True
$AUTO_PUSHD = True
# default: $SHELL_TYPE=best.
# Goes for prompt_toolkit, if available and readline if not
$AUTO_SUGGEST_IN_COMPLETIONS = True
$CASE_SENSITIVE_COMPLETIONS = ''
$DYNAMIC_CWD_ELISION_CHAR = '…'
$XONSH_GITSTATUS_HASH = ':'
$XONSH_GITSTATUS_BRANCH = ''
$XONSH_GITSTATUS_OPERATION = ''
$XONSH_GITSTATUS_STAGED = '●'
$XONSH_GITSTATUS_CONFLICTS = '×'
$XONSH_GITSTATUS_CHANGED = '+'
$XONSH_GITSTATUS_UNTRACKED = '?'
$XONSH_GITSTATUS_STASHED = '⚑'
$XONSH_GITSTATUS_CLEAN = '✓'
$XONSH_GITSTATUS_AHEAD = '↑·'
$XONSH_GITSTATUS_BEHIND = '↓·'
# $XONSH_SHOW_TRACEBACK = True
$XONSH_TRACEBACK_LOGFILE = '/tmp/xonsh-traceback.log'
$VI_MODE = False
$PROMPT = (
  "{env_name}{BOLD_GREEN}{user}@{hostname} "
  "{BOLD_BLUE}{short_cwd} "
  "{branch_color}{gitstatus}{RESET}\n"
  "{BOLD_BLUE}{prompt_end} {RESET}"
)
# $COMPLETIONS_CONFIRM=True
# source-zsh "echo loading xonsh foreign shell"
# xontrib load autojump base16_shell distributed jedi mpl prompt_ret_code whole_word_jumping vox fzf-widgets
# XONSH WIZARD END

# autojump: xpip install xontrib-autojump
# if xontribs.find_xontrib("autojump"):
# if xontribs.find_xontrib("vox"):

@events.on_post_rc
def _xontrib_load():
    # xontrib load whole_word_jumping
    # xontrib load autojump
    xontrib load zoxide

xontrib load vox autovox abbrevs

_home = Path.home()

source-bash --overwrite-aliases ~/.profile

# zdotdir = Path(os.getenv('ZDOTDIR', $HOME)
# zshrc_base = zdotdir / '.zshrc-base'
# source-zsh --suppress-skip-message @(zshrc_base)
# zshrc_local = zdotdir / ('.zshrc-' + gethostname())
# source-zsh --suppress-skip-message @(zshrc_local)

if 'xonsh' not in $SHELL:
    $SHELL = _home / '.local/bin/xonsh'

# Vim
$VIM_COLOR = 'gruvbox'
$VIM_PLUG = 1

# yadm
if (_home / '.yadm-project').is_dir():
    $BASH_COMPLETIONS.append(_home / '.yadm-project/completion/yadm.bash_completion')

# pyenv
if not os.getenv("PYENV_ROOT"):
    $PYENV_ROOT = $HOME + "/.pyenv"

_pyenv_root = Path($PYENV_ROOT)

if _pyenv_root.is_dir() and not all(str(_pyenv_root) in path for path in $PATH):
    $PATH.insert(0, _pyenv_root / "shims")
    $PATH.insert(0, _pyenv_root / "bin")
    $PYENV_SHELL = "python"
    $BASH_COMPLETIONS.append(_pyenv_root / "completions/pyenv.bash")
    # Buggy: https://github.com/xonsh/xonsh/issues/3244
    # Update: it does not throw a bug but pyenv shell does not work
    # source-bash --suppress-skip-message --overwrite-alias $(pyenv init -)

# FIXME: does not work!
# source-bash --suppress-skip-message $(pyenv virtualenv-init -)
_pyenv_virtualenv = _pyenv_root / "plugins/pyenv-virtualenv"
if _pyenv_virtualenv.is_dir():
    $PATH.insert(0, _pyenv_virtualenv / "shims")
    $PYENV_VIRTUALENV_INIT = 1

# pyenv + vox
$VIRTUALENV_HOME = str(_pyenv_root / 'versions')


@events.on_pre_prompt
def _conda_prompt():
    """Indicate conda prompt on the right."""
    # directory = Path.cwd()
    try:
        conda_prefix = $CONDA_PROMPT_MODIFIER
    except KeyError:
        conda_prefix = ""

    $RIGHT_PROMPT = conda_prefix


# FIXME: Slow pre_cmdloop
# @events.autovox_policy
# def _pyenv_virtualenv(path):
#     """Walk up directory tree and activate virtualenv using vox"""
#     python_version = path / ".python-version"
#     if python_version.exists():
#         pyenv_virtualenv_hook=$(pyenv sh-activate --quiet)
#
#         if pyenv_virtualenv_hook.strip() == "false":
#             with suppress(KeyError):
#                 # Virtual env activated, but outside of a virtual env work
#                 # directory
#                 if $PYENV_VIRTUAL_ENV:
#                     vox = voxapi.Vox()
#                     vox.deactivate()
#                     del $PYENV_VIRTUAL_ENV
#
#             # KeyError => No virtual env activated, no virtual env assigned
#         else:
#             # source-bash --suppress-skip-message @(pyenv_virtualenv_hook)
#             virtualenv = python_version.read_text().strip()
#             $PYENV_VIRTUAL_ENV = virtualenv
#             $VIRTUAL_ENV = virtualenv
#             return virtualenv

# Clean up
del _pyenv_root, _pyenv_virtualenv, _home

# >>> mamba initialize >>>
# !! Contents within this block are managed by 'mamba init' !!
$MAMBA_EXE = "/home/avmo/.nix-profile/bin/micromamba"
# $MAMBA_EXE = "/home/avmo/.micromamba/envs/mamba-dev/bin/micromamba"
$MAMBA_ROOT_PREFIX = "/home/avmo/.micromamba"
import sys as _sys
from types import ModuleType as _ModuleType
_mod = _ModuleType("xontrib.mamba",
                   'Autogenerated from $("/usr/bin/micromamba" shell hook -s xonsh -p "/home/avmo/.micromamba")')
__xonsh__.execer.exec($(@($MAMBA_EXE) "shell" "hook" -s xonsh -p "/home/avmo/.micromamba"),
                      glbs=_mod.__dict__,
                      filename='$("/home/avmo/.nix-profile/bin/micromamba" shell hook -s xonsh -p "/home/avmo/.micromamba")')
_sys.modules["xontrib.mamba"] = _mod
del _sys, _mod, _ModuleType
# <<< mamba initialize <<<

# _conda = _pyenv_root / 'versions/miniconda3-latest/bin/conda'
# _conda = Path('/opt/miniconda3/bin/conda')
# _conda = Path.home() / ".micromamba/bin/conda"
# # >>> conda initialize >>>
# # !! Contents within this block are managed by 'conda init' !!
# # Generalized with _conda path object
# import sys as _sys
# from types import ModuleType as _ModuleType
# _mod = _ModuleType("xontrib.conda",
#                    "Autogenerated from $({} shell.xonsh hook)".format(_conda))
# __xonsh__.execer.exec($(@(_conda) "shell.xonsh" "hook"),
#                       glbs=_mod.__dict__,
#                       filename="$({} shell.xonsh hook)".format(_conda))
# _sys.modules["xontrib.conda"] = _mod
# del _sys, _mod, _ModuleType, _conda
# # <<< conda initialize <<<
# # conda xontrib overrides system python
# conda deactivate

# ssh-agent
# _SSH_AGENT_ENV=Path($XDG_RUNTIME_DIR + "/ssh-agent.env")
_SSH_AGENT_ENV=Path.home() / ".cache/prezto/ssh-agent.env"
if not $(pgrep -u $USER ssh-agent):
    _SSH_AGENT_ENV.write_text($(ssh-agent -s))
else:
  if _SSH_AGENT_ENV.exists():
      source-bash @(_SSH_AGENT_ENV)
  else:
      print(
        "error: ssh-agent is running but _SSH_AGENT_ENV not found! killall ssh-agent"
      )

# nvim in xonsh bug
# https://github.com/xonsh/xonsh/issues/3672
$THREAD_SUBPROCS = False


# snek5000
def _cd_snek():
    # avoiding os.chdir to make xonsh trigger events
    cd "$HOME/src/snek5000/snek5000-abl"


def _workon_snek():
    _cd_snek()
    source-bash --suppress-skip-message activate.sh


def _ve():
    venv = Path.cwd() / "venv"
    if not venv.exists():
        python -m venv venv

    if $(which vox):
        vox activate venv
    else:
        source-bash venv/bin/activate


aliases['cd-snek'] = _cd_snek
aliases['workon-snek'] = _workon_snek
aliases['ve'] = _ve
aliases['vd'] = 'vox deactivate'


def _multi_dot_transform(multi_dot):
    """Convert

    ... -> ../..
    .... -> ../../..

    """
    nb_levels = multi_dot.count(".") - 1
    return os.sep.join(("..",) * nb_levels)


def _z_dot_to_parent_completer(prefix, line, begidx, endidx, ctx):
    """Transform command arguments containing ...* to ../..*"""
    if prefix.startswith("..."):
        suggestion = _multi_dot_transform(prefix)
        last_dot_idx = prefix.rfind(".") + 1
        return {suggestion + prefix[last_dot_idx:]}


completer add "z_dot_to_parent" _z_dot_to_parent_completer


# for ndots in range(3, 7):
#     abbrevs['.' * ndots] = '../' * (ndots - 1)

abbrevs['g'] = 'git'
abbrevs['ipy'] = 'ipython'
abbrevs['py'] = 'python'
abbrevs['pac'] = 'pacman'
abbrevs['pacs'] = 'pacsearch'
abbrevs['pacq'] = 'pacman -Q'
abbrevs['paci'] = 'sudo pacman -S'
abbrevs['pacu'] = 'checkupdates -d'
abbrevs['pacU'] = 'sudo pacman -Syu'
abbrevs['pacx'] = 'sudo pacman -R'
abbrevs['pacX'] = 'sudo pacman -Rsnc'
