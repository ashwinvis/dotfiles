pip install ipympl cite2c jupyterlab-git jupytext jupyter_contrib_nbextensions

python -m cite2c.install

jupyter contrib nbextension install --user

jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter labextension install jupyter-matplotlib

pip install hide_code
jupyter nbextension install --py hide_code --user
jupyter nbextension enable --py hide_code --user
jupyter serverextension enable --py hide_code --user
