local custom_modules=(
  'zsh-autosuggestions'
  'zsh-syntax-highlighting'
)
for mod in $custom_modules
  source ${0:A:h}/$mod/$mod.plugin.zsh
