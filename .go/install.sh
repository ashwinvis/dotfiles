#!/bin/zsh
go get -u github.com/errata-ai/vale

go get -u github.com/msprev/fzf-bibtex/cmd/bibtex-ls
go install github.com/msprev/fzf-bibtex/cmd/bibtex-ls@latest
go install github.com/msprev/fzf-bibtex/cmd/bibtex-markdown@latest
go install github.com/msprev/fzf-bibtex/cmd/bibtex-cite@latest

# go get github.com/RasmusLindroth/i3keys
go get -u github.com/zquestz/s

go get -u github.com/muesli/duf
