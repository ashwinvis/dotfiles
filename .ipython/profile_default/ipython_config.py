import os


#  os.environ["QT_QPA_PLATFORM"] = "xcb"
del os
c = get_config()
# c.InteractiveShellApp.pylab = 'auto'
# c.InteractiveShellApp.extensions = ['autoreload']
c.InteractiveShellApp.exec_lines = [
    # '%pylab --no-import-all',
    'try: import numpy as np, matplotlib.pyplot as plt\nexcept ImportError: pass\n'
    #  '%autoreload 2',
]
# c.InteractiveShellApp.exec_lines.append('print("Warning: disable autoreload in ipython_config.py to improve performance.")')

# matplotlib
c.InlineBackend.rc = {}
# HIDPI
# c.InlineBackend.figure_format = 'retina'

c.IPCompleter.use_jedi = False
c.Completer.use_jedi = False
