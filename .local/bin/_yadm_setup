#!/bin/bash
declare -r SSH_KEY=$HOME/.ssh/git
declare -r SSH_CONFIG=$HOME/.ssh/config
declare -r VERSION=3.1.1

function yes_or_no {
  while true; do
    read -rp "$* [y/n]: " yn
    case $yn in
      [Yy]*) return 0  ;;
      [Nn]*) return 1  ;;
    esac
  done
}

# Command yadm does not exists
mkdir -p __yadm__ && pushd __yadm__
export PATH="$PATH:$PWD"

if [[ -z $(type -P "yadm") ]]; then
  if yes_or_no 'Command yadm not found. Try installing using system package manager?'; then
    if [ -x /usr/bin/apt ]; then
      # Debian / Ubuntu
      sudo apt install yadm
    elif [ -x /usr/bin/paru ]; then
      # ArchLinux
      paru -S yadm
    fi
    hash -r
  else
    echo "Download yadm to $PWD"
    curl -fLo ./yadm  https://raw.githubusercontent.com/TheLocehiliosan/yadm/$VERSION/yadm
    chmod +x ./yadm
  fi
fi

popd


echo "Setup ssh"
mkdir -p ~/.ssh
if [ ! -f $SSH_KEY ]; then
  ssh-keygen -t ed25519 -C "$(whoami)@$(hostname)-$(date -I)" -f $SSH_KEY
  echo "Generated ssh key $SSH_KEY.pub:"
else
  echo "Using pre-existing ssh key $SSH_KEY.pub:"
fi

cat "$SSH_KEY.pub"

yes_or_no "Waiting for the user to copy the ssh-key to the dotfiles repo... "

for yadm_dir in ~/.yadm ~/.config/yadm
do
  if [ -d $yadm_dir ]; then
    yes_or_no "Remove existing $yadm_dir? " && rm -rf $yadm_dir
  fi
done

if [ ! -f $SSH_CONFIG ]; then
  echo "Creating a minimal $SSH_CONFIG"
  touch ~/
  cat>>$SSH_CONFIG<<EOF
Host github.com gitlab.com codeberg.org framagit.org
IdentitiesOnly yes
IdentityFile $SSH_KEY

# Intermediate: Host keys the client accepts - order here is honored by OpenSSH
HostKeyAlgorithms ssh-ed25519-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ssh-ed25519,ssh-rsa,ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521,ecdsa-sha2-nistp384,ecdsa-sha2-nistp256
EOF

fi

echo "Cloning dotfiles repo"
yadm clone git@codeberg.org:ashwinvis/dotfiles.git

echo "Done!"
