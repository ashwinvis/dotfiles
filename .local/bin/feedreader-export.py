#!/usr/bin/env python
# Used as a quick script to export current feeds from feedreader in order
# to move/recreate the current feed configuration
import sqlite3
from pathlib import Path
from datetime import datetime

HOME = Path.home()
DB_PATH = HOME / ".local/share/feedreader/data/feedreader-7.db"
EXPORT_PATH = HOME / (
    "Nextcloud/AppData/nextcloud-news/subscriptions-"
    f"{datetime.today().isoformat().partition('T')[0]}.opml"
)

print("Using database: {}".format(DB_PATH))
db = sqlite3.connect(DB_PATH)
cursor = db.cursor()
cursor.execute(
    "select name,url,xmlURL,categories.title as category from feeds LEFT JOIN categories ON feeds.category_id=categories.categorieID ORDER BY category"
)
formatting = [
    '<?xml version="1.0" encoding="UTF-8"?>\n',
    '<opml version="2.0">\n',
    "<head>\n",
    "\t<title>FeedReader Exported RSS Feeds</title>\n",
    "</head>\n",
    "<body>\n",
]
exports = open(EXPORT_PATH, "w")

exports.writelines(formatting)
output = cursor.fetchall()
categories = {line[3]: "" for line in output}
for title, url, rss, category in output:
    categories[
        category
    ] += '\t\t<outline title="{0}" text="{0}" type="rss" htmlUrl="{1}" xmlUrl="{2}"/>\n'.format(
        title, url, rss
    )

for category, feeds in categories.items():
    exports.write('\t<outline title="{0}" text="{0}">\n'.format(category))
    exports.write(feeds)
    exports.write("\t</outline>\n")

exports.write("\t</body>\n")
exports.write("</opml>\n")
exports.close()

print("Exported to {}".format(exports.name))
