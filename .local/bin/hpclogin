#!/bin/bash
# Fail on error, unbound variables, piped errors
set -euo pipefail


set_title() {
        title=$1
        if [ $COLORTERM=="gnome-terminal" ]; then
            echo -ne '\033]2;'$title'\033\\'
        else
            echo -ne "\033]0;$title\007"
        fi
}

function yes_or_no {
  while true; do
    read -p "$* [y/N]: " yn
    yn=${yn:-N}  # Default: No -- delete this for no defaults
    case $yn in
      [Yy]*) return 0  ;;
      [Nn]*) echo "Skipping login" ; return  1 ;;
    esac
  done
}

kerberos_login() {
  local user=$1
  local domain=$2
  local duration="1d" #12h 10d 1y
  yes_or_no "Initialize Kerberos key" && \
    kinit $user@${domain} && \  #  -l $duration, If forwardable add -f
    klist -f
}


servers=(
  "Quit"
  # "pelvoux.mech.kth.se"
  "tetralith-squeue"
  "tetralith-jlab"
  "tetralith-tunnel"
  "tetralith.nsc.liu.se"
  "tetralith1.nsc.liu.se"
  "tetralith2.nsc.liu.se"
  "su.se"
  "ana.noho.st"
  "kebnekaise.hpc2n.umu.se"
  # "beskow.pdc.kth.se"
  "tegner.pdc.kth.se"
  # "abisko.hpc2n.umu.se"
  "dt01.bsc.es"
  "mt1.bsc.es"
  "mt2.bsc.es"
)
PS3="Enter the HPC server option : "
select srv in "${servers[@]}"
do
    case $srv in
        "Quit")
            set_title $(hostname -s)
            break
            ;;
        "pelvoux.mech.kth.se")
            set_title $srv
            kerberos_login avmo MECH.KTH.SE
            ssh -CY avmo@$srv
            ;;
        "tetralith.nsc.liu.se")
            set_title $srv
            ssh -CY x_ashmo@$srv
            ;;
        "tetralith1.nsc.liu.se")
            set_title $srv
            ssh -CY x_ashmo@$srv
            ;;
        "tetralith-squeue")
            set_title $srv
            ssh x_ashmo@tetralith.nsc.liu.se <<-ENDSSH
            squeue -u x_ashmo
ENDSSH
            ;;
        "tetralith-jlab")
            set_title $srv
            echo "Setting up JupyterLab inside a remote tmux session..."
            ssh x_ashmo@tetralith.nsc.liu.se <<-ENDSSH
            bash -lc "
              set -x
              tmux kill-session -t jlab;
              tmux new -s jlab -d;
              tmux send -t jlab 'workon-snek; snakemake jlab -j' ENTER
            "
ENDSSH
            ;;
        "tetralith-tunnel")
            set_title $srv
            read -p "Node [localhost]:" node
            node=${node:-"localhost"}
            read -p "Enter port to tunnel [5656]: " tport
            tport=${tport:-"5656"}
            echo "Setting up SSH tunnel inside a local tmux session..."

            SESSION="ssh_tunnel_${node}_${tport}"
            set +e
            tmux has -t $SESSION
            if [ $? != 0 ]
            then
              tmux new  -s $SESSION -d
              tmux send -t $SESSION "
                ssh -v -N -L localhost:$tport:$node:$tport x_ashmo@tetralith.nsc.liu.se;
                echo Tunnel terminated;
                exit" ENTER
            else
              echo "Session $SESSION already exists"
            fi
            set -e

            yes_or_no "Attach to tmux session?" && tmux attach || tmux ls
            ;;
        "tetralith2.nsc.liu.se")
            set_title $srv
            ssh -CY x_ashmo@$srv
            ;;
        "beskow.pdc.kth.se")
            set_title $srv
            kerberos_login avmo NADA.KTH.SE
            ssh -CY avmo@$srv
            ;;
        "tegner.pdc.kth.se")
            set_title $srv
            kerberos_login avmo NADA.KTH.SE
            ssh -CY avmo@$srv
            ;;
        "abisko.hpc2n.umu.se")
            set_title $srv
            ssh -CY avmo@$srv
            ;;
        "kebnekaise.hpc2n.umu.se")
            set_title $srv
            ssh -CY avmo@$srv
            ;;
        "dt01.bsc.es")
            set_title $srv
            ssh nct01032@$srv
            ;;
        "mt1.bsc.es")
            set_title $srv
            ssh nct01032@$srv
            ;;
        "mt2.bsc.es")
            set_title $srv
            ssh nct01032@$srv
            ;;
        "su.se")
            set_title $srv
            declare -r me="asmo4461" # "$(whoami)"
            declare -r src="${me:0:1}"/"${me:1:1}"/"${me}"
            kerberos_login asmo4461 SU.SE && \
              echo "Mounting /mnt/X drive" && \
              sudo mount.cifs //win.su.se/dfs/commonextra/misu.su.se /mnt/X/ -o user=${me},vers=3.0,sec=krb5,cruid=$(id --user) && \
              echo "Mounting /mnt/H drive" && \
              sudo mount.cifs //fileshare02-p.win.su.se/${src} /mnt/H/ -o user=${me},vers=3.0,sec=krb5,cruid=$(id --user)
            ;;
        "ana.noho.st")
            set_title $srv
            ssh -C admin@$srv -p 4988
            ;;
        *) echo invalid option;;
    esac
done
