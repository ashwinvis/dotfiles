#!/usr/bin/env python
# Requires urllib, lxml, html5lib

import urllib.request
from io import StringIO
from lxml import etree
from lxml.html import html5parser as html5p
import pandas as pd


pd.options.display.max_colwidth = 100


def eprint(element_tree):
    print(etree.tostring(element_tree, pretty_print=True))


def dprint(dico):
    for k, v in dico.items():
        fmt = '{:<8}' + '  {:<15}' * len(v)
        print(fmt.format(k, *v))


def tree_from_url(_url, display=False, html5=False):
    # get website content
    req = urllib.request.Request(url=_url)
    f = urllib.request.urlopen(req)
    html = f.read().decode('utf-8')

    # parse with lxml to from element tree
    if html5:
        parser = html5p.HTMLParser()
        tree = html5p.parse(StringIO(html), parser)
    else:
        parser = etree.HTMLParser()
        tree = etree.parse(StringIO(html), parser)
    if display:
        eprint(tree)

    return tree


url = {'q': 'http://www.hors.se/veckans-meny/?l=e',
       'b': 'https://brazilia.gastrogate.com/lunch/',
       'n': 'http://ths.kth.se/om-ths/restaurang-cafe/restaurang-nymble-meny/',
       'c': 'http://www.kvartersmenyn.se/rest/14912'
       }

tree = dict.fromkeys(url.keys())
menu = dict.fromkeys(url.keys())


def q(k='q'):
    if tree[k] is None:
        tree.update({k: tree_from_url(url[k])})

    days = tree[k].xpath('//th[@class="amatic-700 whoa text-danger"]/text()')[:5]
    menu_list = tree[k].xpath('//td/text()')

    menu[k] = {'menu1': menu_list[0:15:3],
               'menu2': menu_list[1:15:3],
               'menu3': menu_list[2:15:3]}

    menu[k] = pd.DataFrame(menu[k])
    menu[k].index = days
    print('\nQ MENY')
    print(menu[k])  # .stack())


def brazilia(k='b'):
    if tree[k] is None:
        tree.update({k: tree_from_url(url[k])})

    days = tree[k].xpath('//th[@class="menu_header"]/text()')
    menu_list = tree[k].xpath('//td[@class="td_title"]/text()')

    days = [d.strip() for d in days]
    menu[k] = pd.DataFrame(menu_list)  # , index=days)

    print('\nBRAZILIA MENY')
    # print(menu[k])
    for d in days:
        print(d.strip('\t\n'))

    for m in menu_list:
        print(m.strip('\t\n'))


def nymble(k='n'):
    if tree[k] is None:
        tree.update({k: tree_from_url(url[k], html5=True)})

    days = tree[k].xpath('//div[@class="ng-binding"]/p/strong/text()')
    menu_list = tree[k].xpath('//div[@class="ng-binding"]/p/text()')

    menu[k] = pd.DataFrame(menu_list)  # , index=days)
    print('\nNYMBLE MENY')
    print(menu[k])


def cypern(k='c'):
    if tree[k] is None:
        tree.update({k: tree_from_url(url[k])})

    days = ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday')  # tree[k].xpath('//div[@class="meny"]/b/text()')
    menu_list = tree[k].xpath('//div[@class="meny"]/text()')

    menu[k] = {'menu1': menu_list[0:20:4],
               'menu2': menu_list[1:20:4],
               'menu3': menu_list[2:20:4],
               'menuveg': menu_list[3:20:4],
               }

    menu[k] = pd.DataFrame(menu[k], index=days)
    print('\nCYPERN MENY')
    print(menu[k])


if __name__ == '__main__':
    cypern()
    #  brazilia()
    q()
