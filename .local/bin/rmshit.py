#! /usr/bin/env python

import os
import shutil
import time
import datetime
import shlex
import subprocess


shittyfiles = [
    "~/.FRD/links.txt",  # FRD
    "~/.FRD/log/app.log",  # FRD
    "~/.adobe",  # Flash crap
    "~/.bazaar/",  # bzr insists on creating files holding default values
    "~/.bzr.log",
    "~/.cache/chromium",
    "~/.cache/pacaur",
    "~/.cache/spotify",
    "~/.config/enchant",
    "~/.dbus",
    "~/.distlib/",  # contains another empty dir, don't know which software creates it
    "~/.dropbox-dist",
    "~/.esd_auth",
    "~/.gconf",
    "~/.gconfd",
    "~/GPUCache",  # Qutebrowser artifact
    "~/.gstreamer-0.10",
    "~/.java/",
    "~/.jssc/",
    "~/.local/share/gegl-0.2",
    "~/.local/share/recently-used.xbel",
    "~/.macromedia",  # Flash crap
    "~/.npm/",  # npm cache
    "~/.nv/",
    "~/.objectdb",  # FRD
    "~/.oracle_jre_usage/",
    "~/.parallel",
    "~/.pulse",
    "~/.pylint.d/",
    "~/.recently-used",
    "~/.spicec",  # contains only log file; unconfigurable
    "~/.thumbnails",
    "~/.tox/",  # cache directory for tox
    "~/.viminfo",  # configured to be moved to ~/.cache/vim/viminfo, but it is still sometimes created...
    "~/.zcompdump",  # make compinit faster
    "~/VideoDecodeStats",
    "~/ca2",  # WTF?
    "~/ca2~",  # WTF?
    "~/databases-incognito",  # created by falkon / qutebrowser
    "~/nltk_data",  # acronym
    "~/nheko-backtrace.dump",  # nheko
    "~/logs",  # Konversation logs
]

cachedirs = ["~/.cache/pip/", "~/.cache/vim/undo/", "~/.cache/nvim/undo/"]

janitors = [
    "flatpak uninstall --unused",
    "cargo cache --autoclean",
    "yarn cache clean",
    "conda clean -ay",
    "nix-env --delete-generations 60d",
    "nix-store --gc",
    f"paccache -rk1 -c {os.path.expanduser('~')}/.cache/makepkg/packages",
    f"paccache -rk1 -c {os.path.expanduser('~')}/.cache/makepkg/repo"
]

sudo_janitors = [
    "paccache -r",
    "paccache -rk1 -c /var/cache/makepkg",
    "paccache -ruk0",
]


term_colors = dict(
    HEADER="\033[95m",
    OKBLUE="\033[94m",
    OKGREEN="\033[92m",
    WARNING="\033[93m",
    FAIL="\033[91m",
    ENDC="\033[0m",
    BOLD="\033[1m",
    UNDERLINE="\033[4m",
)


def cstring(*args, **kwargs):
    try:
        color = kwargs.pop("color")
    except KeyError:
        color = "OKGREEN"

    return term_colors[color] + " ".join(args) + term_colors["ENDC"]


def cprint(*args, **kwargs):
    print(cstring(*args, **kwargs))


def is_old(path, age_in_days=120):
    mtime = os.path.getmtime(path)
    age_in_seconds = age_in_days * datetime.timedelta(days=1).total_seconds()
    oldtime = time.time() - age_in_seconds

    return mtime < oldtime


def find_old_files(path):
    path = os.path.expanduser(path)
    found = set()
    for root, dirs, files in os.walk(path):
        for name in files:
            filepath = os.path.join(root, name)
            if is_old(filepath):
                found.add(filepath)
    return found


def yesno(question, default="n"):
    """yesno
    Asks the user for YES or NO, always case insensitive.
    Returns True for YES and False for NO.

    Parameters
    ----------

    question : str
    default : str

    Returns
    -------
    bool

    """
    prompt = cstring("{} (y/[n]) ".format(question), color="FAIL")

    ans = input(prompt).strip().lower()

    if not ans:
        ans = default

    if ans == "y":
        return True
    return False


def is_exe(fpath):
    #  return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
    return shutil.which(fpath) is not None


def execute_janitors(janitors):
    for cmd in janitors:
        if is_exe(shlex.split(cmd)[0]):
            print(cmd)
            subprocess.run(shlex.split(cmd))


def print_list(lst):
    cprint(" -", "\n - ".join(lst))


def rmshit():
    cprint("Janitors:")
    print_list(janitors)
    if yesno("Execute janitor programs?", default="n"):
        execute_janitors(janitors)

    if os.getenv("SUDO_USER"):
        cprint("Super user janitors:")
        print_list(sudo_janitors)
        if yesno("Execute super user janitors?"):
            execute_janitors(sudo_janitors)

    found = []
    for f in shittyfiles:
        absf = os.path.expanduser(f)
        if os.path.exists(absf):
            found.append(absf)
            # print("    %s" % f)

    for d in cachedirs:
        found.extend(find_old_files(d))

    if found:
        cprint("Found shittyfiles:")
        print_list(found)
    else:
        print("No shitty files found :)")
        return

    if yesno(f"Remove all {len(found)} items?", default="n"):
        for f in found:
            if os.path.isfile(f):
                os.remove(f)
            else:
                shutil.rmtree(f)
        print("All cleaned")
    else:
        print("No file removed")


if __name__ == "__main__":
    rmshit()
