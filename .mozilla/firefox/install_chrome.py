from configparser import ConfigParser
from pathlib import Path


profiles = ConfigParser()
here = Path(__file__).parent
profiles.read(here / "profiles.ini")


for section in profiles.sections():
    if profiles.has_option(section, "Path"):
        profile_path = here / profiles.get(section, "Path", fallback="")
        chrome = "../chrome"
        symlink = profile_path / "chrome"
        if profile_path.exists() and not symlink.exists():
            symlink.symlink_to(
                chrome,
                target_is_directory=True
            )
            print(profile_path / chrome, "is now installed as", symlink.resolve())
        else:
            print(profile_path / chrome, "is already installed")
