import os
import shutil
from pathlib import Path


thunderbird = Path.home() / ".thunderbird"
backup_dest = thunderbird / "backup"

backup_list = ("msgFilterRules.dat",)


def backup():
    to_backup = []
    for root, dirs, files in os.walk(thunderbird):
        to_backup.extend(Path(root) / f for f in files if f in backup_list)

    # print(to_backup)
    for file in to_backup:
        profile = next(p for p in thunderbird.iterdir() if p in file.parents)
        dest = backup_dest / file.relative_to(profile)
        dest.parent.mkdir(parents=True, exist_ok=True)
        if file != dest:
            print(f"Copy {file} -> {dest}")
            shutil.copy2(file, dest)


if __name__ == "__main__":
    backup()
