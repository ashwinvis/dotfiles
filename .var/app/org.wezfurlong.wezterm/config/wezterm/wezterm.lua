local wezterm = require 'wezterm'
local config = {}
local hour = tonumber(os.date("%H"))

if hour > 6 and hour < 18 then
  config.color_scheme = 'One Light (base16)'
  config.set_environment_variables = {
    DELTA_FEATURES = 'my-light-mode',
  }
else
  config.color_scheme = 'One Dark (base16)'
  config.set_environment_variables = {
    DELTA_FEATURES = 'my-dark-mode',
  }
end

return config
