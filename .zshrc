# Set up the prompt

autoload -Uz promptinit
promptinit
# prompt adam1
prompt off

setopt histignorealldups sharehistory
# do not error if glob matches fail
unsetopt nomatch

# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format '%B%F{magenta}--- %d ---%f%b'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

eval "$(starship init zsh)"

# Customize to your needs...

# Load custom modules
source "${HOME}/.config/zsh/modules/init.zsh"

# System-specific customizations
# Different ways to profile
# zmodload zsh/zprof
# zprof

# REPORTTIME=0

# FIXME: for timing
# zmodload zsh/datetime
# export PS4='${(j::)epochtime} %N:%i> '  # nanoseconds
# setopt xtrace

[ -z "${USER_SHELL_PROFILE_INIT}" ] && [ -f ~/.config/interactive.sh ] && . ~/.config/interactive.sh

_ZSHRC_BASE="${HOME}/.config/zsh/runcoms/base"
_ZSHRC_LOCAL="${HOME}/.config/zsh/runcoms/$HOST"

if [ -f $_ZSHRC_BASE ]; then
  source $_ZSHRC_BASE
fi

if [ -f $_ZSHRC_LOCAL ]; then
  source $_ZSHRC_LOCAL
fi
# FIXME: for timing
# unsetopt xtrace


eval $(atuin init zsh)
