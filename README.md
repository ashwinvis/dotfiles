# Ashwin's Linux dotfiles #
A repository for miscellaneous Linux configurations to be deployed in a `$HOME` directory.
The dotfiles are managed using [yadm](https://thelocehiliosan.github.io/yadm/).

### Dependencies
- bash / zsh
- curl
- git
- inetutils (for `hostname` command)
- yadm

### How do I get set up  ?

Install yadm [by following the
docs](https://thelocehiliosan.github.io/yadm/docs/install) or simply

```sh
mkdir /tmp/__yadm__
cd /tmp/__yadm__
export PATH=$PATH:$PWD
curl -L https://codeberg.org/ashwinvis/dotfiles/raw/master/.local/bin/_yadm_setup -o _yadm_setup
chmod +x ./_yadm_setup
hash -r
_yadm_setup
```

Now you should have a `yadm` command symlinked to `$HOME/.local/bin/yadm`

### Cheatsheet for common commands

#### Conflict resolution

Fixing first time conflicts (**NOTE**: run `yadm bootstrap` only after all conflicts are resolved)

```sh
yadm stash show
yadm stash pop
```

- Restore one by one
```sh
yadm checkout -- <file> / <directory>
```

- Automatically restore all "deleted"
```sh
yadm status --short | awk '/^ D/{print $2}' | xargs -P4 yadm checkout --
```

#### Initializing submodules
```sh
yadm submodule update --init --recursive
```

### Provides configurations for
- vim : plug
- i3
- XTerm: rofi, [font awesome](http://fontawesome.io/cheatsheet/)
- Conky
- zsh
- pyenv
- ssh
...

## License

                     GNU GENERAL PUBLIC LICENSE
                      Version 3, 29 June 2007

    Linux dotfiles
    Copyright (C) 2019  Ashwin Vishnu Mohanan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

  You should also get your employer (if you work as a programmer) or school,
if any, to sign a "copyright disclaimer" for the program, if necessary.
For more information on this, and how to apply and follow the GNU GPL, see
<http://www.gnu.org/licenses/>.

  The GNU General Public License does not permit incorporating your program
into proprietary programs.  If your program is a subroutine library, you
may consider it more useful to permit linking proprietary applications with
the library.  If this is what you want to do, use the GNU Lesser General
Public License instead of this License.  But first, please read
<http://www.gnu.org/philosophy/why-not-lgpl.html>.

A sample sentence with a link and containing
a lot of text.
